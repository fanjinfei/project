#!/usr/bin/env python
import sys, os, time
from pprint import pprint
import libcloud

from libcloud.storage.types import Provider
from libcloud.storage.providers import get_driver

import libcloud.security
libcloud.security.VERIFY_SSL_CERT = False

#import urllib3
#urllib3.disable_warnings()
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

user,apikey,container_name = sys.argv[1], sys.argv[2], sys.argv[3]
proxy= os.environ['http_proxy']

''' Compute Node
cls= libcloud.get_driver(libcloud.DriverType.COMPUTE, libcloud.DriverType.COMPUTE.AZURE)
driver= cls(user, apikey, http_proxy=proxy)
pprint(driver.list_sizes())
pprint(driver.list_nodes())
'''
#cls= libcloud.get_driver(libcloud.DriverType.COMPUTE, libcloud.DriverType.COMPUTE.RACKSPACE)

# Storage
import libcloud
from libcloud.storage.types import Provider
from libcloud.storage.providers import get_driver

cls = get_driver(Provider.AZURE_BLOBS)
driver= cls(user, apikey, secure=False, http_proxy=proxy) #libcloud.storage.drivers.azure_blobs.AzureBlobsStorageDriver
container = driver.get_container(container_name)
for obj in driver.iterate_container_objects(container):
    break
    driver.download_object(obj, '/tmp/a.out', overwrite_existing=True) # NOT working if secure=True in cls()
    break

time.sleep(7)
print('downloading..')
obj = driver.get_object(container_name, 'resources/788fa176-3fbc-11e7-a8a1-0344bb6b0ee1/tobedeletedtest.csv')
print ('download file size', obj.size)
driver.download_object(obj, '/tmp/test2.csv', overwrite_existing=True) # NOT working if secure=True in cls()
print('downloaded')

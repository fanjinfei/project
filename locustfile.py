from locust import HttpLocust, TaskSet, task
from locust import main as locustMain
import argparse,sys,imp

#used by main
import locust
from locust import runners

import gevent
import sys
import os
import signal
import inspect
import logging
import socket
from optparse import OptionParser

from locust import web
from locust.log import setup_logging, console_logger
from locust.stats import stats_printer, print_percentile_stats, print_error_report, print_stats
from locust.inspectlocust import print_task_ratio, get_task_ratio_dict
from locust.core import Locust, HttpLocust
from locust.runners import MasterLocustRunner, SlaveLocustRunner, LocalLocustRunner
from locust import events

'''
virtualenv ./
pip install locustio, pyzmq
locust -f locustfile.py --host=http://localhost:5000
open broswer http://localhost:8089
'''

version = locust.__version__

global package_id, username, password
#OR rewrite locust.main
username = 'adf' #raw_input("please enter username:")
password = 'asdf' #raw_input("please enter password:")
package_id = 'safdasdf'


def login(l, username, password):
    l.client.post("/login_generic", {"login":username, "password":password})

def index(l):
    l.client.get("/dataset")

class ForumPage(TaskSet):
    @task(2)
    def read_thread(self):
        response = self.client.get("/organization")

    @task(1)
    def new_thread(self):
        response = self.client.get("/organization/new")
        if response.status_code != 200:
            print ("Response content:", response.content)

    @task(5)
    def stop(self):
        self.interrupt()

class UserBehavior(TaskSet):
    tasks = {index:2, ForumPage:5}
    #tasks = {profile:1}

    def on_start(self):
        global username, password
        login(self, username, password)

    @task(1)
    def profile(self):
        response = self.client.get("/en/dashboard") #profile
        #response = self.client.get("/about")
        #print "Response status code:", response.status_code
        #print "Response content:", response.content

class classproperty(object):
    """ @classmethod+@property """
    def __init__(self, f):
        self.f = classmethod(f)
    def __get__(self, *a):
        return self.f.__get__(*a)()

class UserBehavior2(TaskSet):

    @task(1)
    def opendata(self):
        response = self.client.get("/dataset")
        #response = self.client.get("/about")
        #print "Response status code:", response.status_code
        #print "Response content:", response.content
    @task(1)
    def data_id(self):
        response = self.client.get("/dataset/"+package_id)
    @task(1)
    def data_api(self):
        response = self.client.get("/api/action/package_show?id="+package_id)
    @task(1) # (0) stop
    def data_hist(self):
        response = self.client.get("/dataset/history/6dc5170d-4167-47e4-b80a-93ed2b47f023?format=atom")

class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    #task_set = UserBehavior2
    min_wait = 5000
    max_wait = 90000

class WebsiteUser2(HttpLocust): #will get constant url, have to be set in definition
    #task_set = UserBehavior2 #meta method save call environment, we can not set dynamic id after this
    min_wait = 5000
    max_wait = 90000

def locustmain(): #re write, hack
    parser, options, arguments = locustMain.parse_options()
    # setup logging
    setup_logging(options.loglevel, options.logfile)
    logger = logging.getLogger(__name__)

    locusts = { 'WebsiteUser': WebsiteUser,
                'WebsiteUser2':WebsiteUser2
                }

    if options.list_commands:
        console_logger.info("Available Locusts:")
        for name in locusts:
            console_logger.info("    " + name)
        sys.exit(0)

    if not locusts:
        logger.error("No Locust class found!")
        sys.exit(1)

    # make sure specified Locust exists
    if arguments:
        missing = set(arguments) - set(locusts.keys())
        if missing:
            logger.error("Unknown Locust(s): %s\n" % (", ".join(missing)))
            sys.exit(1)
        else:
            names = set(arguments) & set(locusts.keys())
            locust_classes = [locusts[n] for n in names]
    else:
        locust_classes = locusts.values()

    if options.show_task_ratio:
        console_logger.info("\n Task ratio per locust class")
        console_logger.info( "-" * 80)
        print_task_ratio(locust_classes)
        console_logger.info("\n Total task ratio")
        console_logger.info("-" * 80)
        print_task_ratio(locust_classes, total=True)
        sys.exit(0)
    if options.show_task_ratio_json:
        from json import dumps
        task_data = {
            "per_class": get_task_ratio_dict(locust_classes),
            "total": get_task_ratio_dict(locust_classes, total=True)
        }
        console_logger.info(dumps(task_data))
        sys.exit(0)

    # if --master is set, make sure --no-web isn't set
    if options.master and options.no_web:
        logger.error("Locust can not run distributed with the web interface disabled (do not use --no-web and --master together)")
        sys.exit(0)

    if not options.no_web and not options.slave:
        # spawn web greenlet
        logger.info("Starting web monitor at %s:%s" % (options.web_host or "*", options.port))
        main_greenlet = gevent.spawn(web.start, locust_classes, options)

    if not options.master and not options.slave:
        runners.locust_runner = LocalLocustRunner(locust_classes, options)
        # spawn client spawning/hatching greenlet
        if options.no_web:
            runners.locust_runner.start_hatching(wait=True)
            main_greenlet = runners.locust_runner.greenlet
    elif options.master:
        runners.locust_runner = MasterLocustRunner(locust_classes, options)
    elif options.slave:
        try:
            runners.locust_runner = SlaveLocustRunner(locust_classes, options)
            main_greenlet = runners.locust_runner.greenlet
        except socket.error, e:
            logger.error("Failed to connect to the Locust master: %s", e)
            sys.exit(-1)

    if not options.only_summary and (options.print_stats or (options.no_web and not options.slave)):
        # spawn stats printing greenlet
        gevent.spawn(stats_printer)

    def shutdown(code=0):
        """
        Shut down locust by firing quitting event, printing stats and exiting
        """
        logger.info("Shutting down (exit code %s), bye." % code)

        events.quitting.fire()
        print_stats(runners.locust_runner.request_stats)
        print_percentile_stats(runners.locust_runner.request_stats)

        print_error_report()
        sys.exit(code)

    # install SIGTERM handler
    def sig_term_handler():
        logger.info("Got SIGTERM signal")
        shutdown(0)
    gevent.signal(signal.SIGTERM, sig_term_handler)

    try:
        logger.info("Starting Locust %s" % version)
        main_greenlet.join()
        code = 0
        if len(runners.locust_runner.errors):
            code = 1
        shutdown(code=code)
    except KeyboardInterrupt as e:
        shutdown(0)

def main():
    global package_id, username, password

    parser = argparse.ArgumentParser()

    parser.add_argument('-u', '--username', dest='username')
    parser.add_argument('-p', '--password', dest='password')
    parser.add_argument('--portal', dest='portal', action='store_true', default=False)
    parser.add_argument('--package_id', dest='package_id')

    args, left = parser.parse_known_args()

    username = args.username or username
    password = args.password or password
    package_id = args.package_id or '6dc5170d-4167-47e4-b80a-93ed2b47f023'
    print("package id: "+ package_id)
    sys.argv = sys.argv[:1]+left

    if args.portal:
        sys.argv.append( 'WebsiteUser2')
    else:
        sys.argv.append( 'WebsiteUser')

    setattr(WebsiteUser2, 'task_set', UserBehavior2) # late binding
    #WebsiteUser2.task_set = UserBehavior2
    print('start real main()')
    WUModule = imp.new_module('WUModule')
    #exec class2str in WUModule.__dict__


    #locustMain.main() #use sys.argv
    # above has problem: it reloads .py file in current directory, rendering args in-effective

    locustmain()

if __name__ =='__main__':
    main()
'''
#run without web/gui
from argparse import Namespace

from locust import runners

import locustfile


options = Namespace()
options.host = "http://localhost"
options.num_clients = 10
options.hatch_rate = options.num_clients
options.num_requests = options.num_clients * 10

runners.locust_runner = runners.LocalLocustRunner([locustfile.MyUser], options)
runners.locust_runner.start_hatching(wait=True)
runners.locust_runner.greenlet.join()

for name, value in runners.locust_runner.stats.entries.items():
    print(name,
          value.min_response_time,
          value.median_response_time,
          value.max_response_time,
          value.total_rps)

'''

'''
#run customized load test
from locust import HttpLocust, TaskSet, task

class UserBehavior(TaskSet):
    global content
    global i
    import os
    os.chdir(".../Downloads/")
    import csv
    with open('auth.csv') as f:
        reader = csv.reader(f)
        content=[]
        for x in reader:
            content.append(x)
        content = [l[0] for l in content]
        print content
    i=0
    @task
    def index(self):
        for i in content:
            self.client.request(method="GET",url="/",
            headers={'Auth-token':i})
    def A(self):
       follow(self)

    @task
    def feedback(self):
       for i in content:
            self.client.request(method="POST",url="/feedback",
            data = {"feedback":"test"},headers={"Auth-Token":i})
   def A(self):
       feedback(self)

class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait=5000
    max_wait=9000
'''

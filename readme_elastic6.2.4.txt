1. install openjdk 10; add PATH
   https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.2.4.tar.gz
   https://artifacts.elastic.co/downloads/kibana/kibana-6.2.4-linux-x86_64.tar.gz
   https://download.java.net/java/GA/jdk10/10.0.1/fb4372174a714e6b8c52526dc134031e/10/openjdk-10.0.1_linux-x64_bin.tar.gz

2. elasticsearch-plugin script: (to install x-pack) (OR with proxy login automator)
   exec "$JAVA" $ES_JAVA_OPTS -DproxyHost=proxy_host -DproxyPort=port -Delasticsearch ...
   bin/elasticsearch-plugin  install x-pack
   bin/x-pack/setup-passwords interactive
   user( elastic, kibana, logstash_system)

   test: http://localhost:9200/

3. https://artifacts.elastic.co/downloads/kibana-plugins/x-pack/x-pack-6.2.4.zip
   bin/kibana-plugin  install file:///home/xxx/es/kibana-plugin-src/x-pack-6.2.4.zip
   modify kibana.yaml
   elasticsearch.username: "kibana"
   elasticsearch.password: "kibanapassword"
   test: http://localhost:5601

4. install x-pack for fess
   http://central.maven.org/maven2/org/codelibs/fess/fess-xpack/12.1.1.3/fess-xpack-12.1.1.3.jar
   cp to app/WEB-INF/lib/
   nano fess-12.1.3/app/WEB-INF/conf/xpack.properties as "xpack.security.user=fess:changeme"
   nano fess-12.1.3/app/WEB-INF/classes/fess_config.properties (cluster name)
   nano fess/bin/fess.in.sh  (external ES lines)
   cp fess/es/plugins/* ../elasticsearch-6.2.4/plugins/
   cp fess/app/WEB-INF/lib/guava-24.0-jre.jar ../elasticsearch-6.2.4/plugins/minhash/
   cp ~/.m2/repository/commons-codec/commons-codec/1.10/commons-codec-1.10.jar <es/plugins/configsync/>
   cp .m2/repository/com/fasterxml/jackson/core/jackson-annotations/2.8.0/jackson-annotations-2.8.0.jar <es/plugins/langfield>
   cp .m2/repository/com/fasterxml/jackson/core/jackson-databind/2.8.10/jackson-databind-2.8.10.jar  <es/plugins/langfield>

   nano elasticsearch.yaml, 
     append line "configsync.config_path: /var/lib/elasticsearch/config"
      http.port: 9400
      transport.tcp.port: 9500-9600

   on ES 6.2, create Role "fess":
curl -XPOST -u elastic 'localhost:9200/_xpack/security/role/fess' -H "Content-Type: application/json" -d '{
  "cluster": ["all"],
  "indices" : [
    {
      "names" : [ "fess*", ".fess*", ".configsync*", ".crawler*", ".suggest*" ],
      "privileges" : [ "all" ]
    }
  ]
}'

  create user "fess":
curl -XPOST -u elastic 'localhost:9200/_xpack/security/user/fess?pretty' -H 'Content-Type: application/json' -d'
{
  "password" : "changeme",
  "roles" : [ "fess" ],
  "full_name" : "Fess"
}'

   file "elasticsearch.yaml": add line "configsync.xpack.security.user: "fess:changeme"

   restart es, kibana, fess


5. build project from pom.xml
   sudo apt install maven
  edit ~/.m2/settings.xml as 
<settings>
  <proxies>
   <proxy>
      <id>example-proxy</id>
      <active>true</active>
      <protocol>http</protocol>
      <host>proxy.example.com</host>
      <port>8080</port>
      <username>proxyuser</username>
      <password>somepassword</password>
      <nonProxyHosts>www.google.com|*.example.com</nonProxyHosts>
    </proxy>
  </proxies>
</settings>

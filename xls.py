# encoding: utf-8
if False:
    import xlwt

    x=1
    y=2
    z=3

    list1=[2.34,4.346,4.234]

    book = xlwt.Workbook(encoding="utf-8")

    #sheet1 = book.add_sheet(u"Sheet 1 lay glish / Nom du ministère en anglais")
    sheet1 = book.add_sheet(u"Sheet 1 lay glish Nom du m")

    sheet1.write(0, 0, "Display glish / Nom du ministère en anglais")
    sheet1.write(1, 0, "Dominance")
    sheet1.write(2, 0, "Test")

    sheet1.write(0, 1, x)
    sheet1.write(1, 1, y)
    sheet1.write(2, 1, z)

    sheet1.write(4, 0, "Stimulus Time")
    sheet1.write(4, 1, "Reaction Time")

    i=4

    for n in list1:
        i = i+1
        sheet1.write(i, 0, n)

    sheet1.col(0).width = 256*35 #35 char

    book.save("trial.xls")


import openpyxl

#book = openpyxl.load_workbook('sheets.xlsx')
book = openpyxl.Workbook()

ws = book.create_sheet(title="April")
for row in range(1, 10):
    ws.append(range(6))
#from openpyxl.cell import get_column_letter
#ws.column_dimensions[0].width = 50
cols = [col for col in ws.columns]
print cols
ws.column_dimensions[cols[0][0].column].width = 50
#cols[0].width = 50


book.create_sheet(title=u"Apri  ministère  l Apri  ministère  l Apri  ministère  l Apri  ministère  l Apri  ministère  l")

print(book.sheetnames)

try:
    sheet1 = book.get_sheet_by_name("Sheet")
    book.remove_sheet(sheet1)
except:
    pass

print(book.sheetnames)

book.create_sheet(title="January", index=0)
print(book.sheetnames)

book.save('sheets2.xlsx')

from openpyxl.chart import (
    PieChart,
    ProjectedPieChart,
    Reference
)
from openpyxl.chart.series import DataPoint

data = [
    ['Pie', 'Sold'],
    ['Apple', 50],
    ['Cherry', 30],
    ['Pumpkin', 10],
    ['Chocolate', 40],
]

wb = openpyxl.Workbook()
ws = wb.active

for row in data:
    ws.append(row)

pie = PieChart()
labels = Reference(ws, min_col=1, min_row=2, max_row=5)
data = Reference(ws, min_col=2, min_row=1, max_row=5)
pie.add_data(data, titles_from_data=True)
pie.set_categories(labels)
pie.title = "Pies sold by category"

# Cut the first slice out of the pie
slice = DataPoint(idx=0, explosion=20)
pie.series[0].data_points = [slice]

ws.add_chart(pie, "D1")


ws = wb.create_sheet(title="Projection")

data = [
    ['Page', 'Views'],
    ['Search', 95],
    ['Products', 4],
    ['Offers', 0.5],
    ['Sales', 0.5],
]

for row in data:
    ws.append(row)

projected_pie = ProjectedPieChart()
projected_pie.type = "pie"
projected_pie.splitType = "val" # split by value
labels = Reference(ws, min_col=1, min_row=2, max_row=5)
data = Reference(ws, min_col=2, min_row=1, max_row=5)
projected_pie.add_data(data, titles_from_data=True)
projected_pie.set_categories(labels)

ws.add_chart(projected_pie, "A10")

from copy import deepcopy
projected_bar = deepcopy(projected_pie)
projected_bar.type = "bar"
projected_bar.splitType = 'pos' # split by position

ws.add_chart(projected_bar, "A27")

print(wb.sheetnames)

wb.save("pie.xlsx")

'''
for col in worksheet.columns:
    max_length = 0
    column = col[0].column # Get the column name
    for cell in col:
        if cell.coordinate in worksheet.merged_cells: # not check merge_cells
            continue
        try: # Necessary to avoid error on empty cells
            if len(str(cell.value)) > max_length:
                max_length = len(cell.value)
        except:
            pass
    adjusted_width = (max_length + 2) * 1.2
    worksheet.column_dimensions[column].width = adjusted_width
'''

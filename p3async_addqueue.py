#!/usr/bin/python3

import asyncio
import random

try:  
    # python 3.4
    from asyncio import JoinableQueue as Queue
except:  
    # python 3.5
    from asyncio import Queue
finish = False
@asyncio.coroutine
def do_work(task_name, work_queue):
    global finish
    while not work_queue.empty() and not finish:
        queue_item = work_queue.get_nowait()

        # simulate condition where task is added dynamically
        if queue_item % 2 != 0:
            qi = 200 + random.randrange(1,30) 
            work_queue.put_nowait(qi)
            print('Added additional {0} item to queue'.format(qi))

        if queue_item == 5: finish =True
        print('{0} got item: {1}'.format(task_name, queue_item))
        yield from asyncio.sleep(1)
        print('{0} finished processing item: {1}'.format(task_name, queue_item))

if __name__ == '__main__':

    queue = Queue()


    # use 3 workers to consume tasks
    taskers = [ 
        do_work('task1', queue),
        do_work('task2', queue),
        do_work('task3', queue)
    ]   

    # Load initial jobs into queue
    [queue.put_nowait(x) for x in range(1, 6)] 

    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.wait(taskers))
    loop.close()

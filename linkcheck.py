#!/usr/bin/env python3
import requests
import json
#from requests import async
import grequests as async
import gevent
from contextlib import closing

url = 'http://github.com'
proxy = 'http://fanjinf:Xinyun99@stcweb.statcan.ca:80/'
proxies = {'http': proxy}

# send string to be received as file
#files = {'file': ('report.csv', 'some,data,to,send\nanother,row,to,send\n')}
# OR files = {'file': open('report.xls', 'rb')}
#r = requests.post(url, files=files)


#payload = {'some': 'data'}
#requests.post(url, json=payload)
#requests.post("http://httpbin.org/post", data=payload) # form encoded
# requests.post(url, data=json.dumps(payload)) # json encoded

#headers = {'user-agent': 'my-app/0.0.1'}
#requests.get(url, headers=headers)

#write r to file
'''
with open(filename, 'wb') as fd:
    for chunk in r.iter_content(chunk_size=128):
        fd.write(chunk)
'''

#response json, content, text
# r.json()  #r.content, r.text

#cookie, Session object
'''
s = requests.Session()
data = {"login":"my_login", "password":"my_password"}
url = "http://example.net/login"
r = s.post(url, data=data)

s.cookies

requests.get(url, cookies=cookies)
'''
try:
    r= requests.get(url, allow_redirects=True, timeout=60,stream=True )
    for line in r.iter_lines():
        pass
    print (r.status_code, r.cookies) #r.headers, 
    if r.status_code != requests.codes.ok:
        r.raise_for_status()
    #raise Exception
except ConnectionError:
    print('err conn')
except requests.exceptions.HTTPError:
    print('err http')
except requests.exceptions.TooManyRedirects:
    print('err maxRdir')
except:
    print('err request')

urls = [
    'http://python-requests.org',
    'http://httpbin.org',
    'http://python-guide.org',
    'http://kennethreitz.com'
]
urls2=[
    'http://localhost:5000/en/dataset/91db3739-3db8-45ca-9a97-3d8a7ce77ae3',
    'http://python-guide.org',
    ]
resps = []
# A simple task to do to each response object
def do_something(response,  *args, **kwargs):
    print (1, response.url, response.status_code, response.history)
    resps.append(response.url)

def do_something2(response,  *args, **kwargs):
    print (2, response.url, response.status_code)

def do_something3(response,  *args, **kwargs):
    print (3, response.url, response.status_code)
    if response.headers.get('Location'):
        print (response.headers.get('Location'))
    ''' BREAEN response 302
    for line in r.iter_lines((chunk_size=10)):
        print (line)
        break
    '''
    if response.status_code == requests.codes.ok:
        count = 0
        for line in response.iter_lines(chunk_size=512):
            count += (len(line))
            if count > 0:
                print( count)
                break

def exception_handler(request, exception):
    print ('Request failed')

pool = async.Pool(10)
jobs = []
reqs=[]
for u in urls2:
    # The "hooks = {..." part is where you define what you want to do
    # 
    # Note the lack of parentheses following do_something, this is
    # because the response will be used as the first argument automatically
    #req = async.get(u, hooks = {'response' : do_something3}, proxies=proxies)
    #req = async.head(u, hooks = {'response' : do_something3})
    req = async.get(u, hooks = {'response' : do_something3}) #read whole body
    #real aysncio, non-blocked
    job = async.send(req, pool, stream=True)
    reqs.append(req)
    jobs.append(job)

def fetch(url):
    response = requests.request('GET', url, timeout=5.0)
    print ("Status: [%s] URL: %s" % (response.status_code, url) )

def _test2__():

    # A list to hold our things to do via async
    async_list = []

    for u in urls:
        # The "hooks = {..." part is where you define what you want to do
        # 
        # Note the lack of parentheses following do_something, this is
        # because the response will be used as the first argument automatically
        action_item = async.get(u, hooks = {'response' : do_something}, proxies=proxies)

        # Add the task to our list of things to do via async
        async_list.append(action_item)

    # Do our list of things to do via async, this is blocked
    async.map(async_list)

    #rs = [async.get(u, hooks = {'response' : do_something2}, proxies=proxies) for u in urls]
    rs = [async.get(u, hooks = {'response' : do_something2},allow_redirects=True, proxies=proxies) for u in urls]
    res = async.map(rs, size=20, stream=True, exception_handler=exception_handler)


    print(res)
    print(resps)
    for u in urls:
        # The "hooks = {..." part is where you define what you want to do
        # 
        # Note the lack of parentheses following do_something, this is
        # because the response will be used as the first argument automatically
        req = async.get(u, hooks = {'response' : do_something3}, proxies=proxies)
        #real aysncio
        job = async.send(req, pool) # OR pool.spawn(fetch, url); pool.join()
        
        jobs.append(job)

gevent.joinall(jobs) #pool.join()
res = [ (v.url, v.response.status_code) for v in reqs]
print(res)

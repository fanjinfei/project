#!/usr/bin/env python

import json
import os
import sys
from lxml import etree
import csv
import unicodecsv
import codecs
import requests
import time
import gzip
from collections import defaultdict

import ckanapi
import ckan
from ckanapi.errors import CKANAPIError
from ckan.logic import (NotAuthorized, NotFound)

import configparser
import psycopg2
import traceback


def write_csv(filename, rows, header=None):
    outf=open(filename, 'wb')
    outf.write(codecs.BOM_UTF8)
    writer = unicodecsv.writer(outf)

    if header:
        writer.writerow(header)
    for row in rows:
        writer.writerow(row)


class GA_Report():
    def __init__(self, conf_file):
        self.file = '/tmp/od-do-canada.jl.gz'
        self.site = ckanapi.RemoteCKAN('http://open.canada.ca/data')
        self.read_orgs()
        self.read_portal()

        user, passwd, host, db = self.read_conf(conf_file)
        db = db.split('?')[0]
        try:
            self.conn = psycopg2.connect(
                database=db, user=user,
                password=passwd, host=host, port="5432")
        except:
            import traceback
            traceback.print_exce()
            print ("Opened database failed")
            sys.exit(-1)

    def read_conf(self,filename):
        config = configparser.ConfigParser()
        config.read(filename)
        psql_conn_str = config.get('app:main', 'sqlalchemy.url')
        import re
        r = re.match(r'^postgresql://(.*):(.*)@(.*)/(.*)', psql_conn_str)
        return (r.group(1), r.group(2), r.group(3), r.group(4))

    def get_deleted_dataset(self, id):
        cur = self.conn.cursor()
        cur.execute('''SELECT a.id, c.value, d.title from package a,
                    package_extra c, public.group d
                    where a.state='deleted' and a.owner_org=d.id
                        and a.id=c.package_id and c.key='title_translated'; ''')
        rows = cur.fetchall()
        for row in rows[:1]:
            id, title, org = row[0], row[1], row[2]
            return (title, org)

        return (None, None)

    def __delete__(self):
        if not self.file:
            if self.download_file:
                os.unlink(self.download_file)
                print('temp file deleted', self.download_file)

    def get_details(self, id):
        try:
            target_pkg = self.site.action.package_show(id=id)
        except:
            target_pkg = None
        return target_pkg

    def read_orgs(self):
        orgs = self.site.action.organization_list(all_fields=True)
        self.orgs = {}
        self.org_name2id = {}

        for rec in orgs:
            title = rec['title'].split('|')
            self.orgs[rec['id']] = title
            self.org_name2id[rec['name']] = rec['id']
        assert(len(self.orgs)>100)
        print 'total orgs ', len(self.orgs)

    def read_portal(self):
        self.ds = {}
        self.org_count = defaultdict(int)
        count = 0
        for records in self.download():
            print 'read records ', count + len(records)
            for rec in records:
                self.ds[rec['id']] = {'title_translated':rec['title_translated'],
                                      'owner_org':rec['owner_org']}
                self.org_count[rec['owner_org']] += 1

    def read_raw_csv(self, filename):
        content=[]
        with open(filename) as f:
            reader = csv.reader(f)
            for x in reader:
                if x:
                    url = x[0].strip()
                    ix1 = url.find('/dataset/')
                    if url[:6] == '/data/' and ix1 > 0:
                        id = url[ix1 + 9:]
                        ix2 = id.find('?')
                        if ix2 > 0:
                            id = id[:ix2]
                        content.append([id, int(x[4])])
        self.content = content

    def process(self, src_dir, dest_dir):

        fname = '/openDataPortal.siteAnalytics.top100Datasets.en.csv'
        self.top100_csv(src_dir + fname, dest_dir + fname)

        fname = '/openDataPortal.siteAnalytics.top100Datasets.fr.csv'
        self.top100_csv(src_dir + fname, dest_dir + fname, 'fr')

        fname = '/openDataPortal.siteAnalytics.datasetsByOrgByMonth.en.csv'
        self.org_stats(src_dir + fname, dest_dir + fname, 'en')

        fname = '/openDataPortal.siteAnalytics.datasetsByOrgByMonth.fr.csv'
        self.org_stats(src_dir + fname, dest_dir + fname, 'fr')

    def org_stats(self, filename, outfname, lang='en'):
        content=[]
        with open(filename) as f:
            reader = csv.reader(f)
            for x in reader:
                content.append(x)
        self.content = content
        
        def get_count(names):
            count = 0
            for name in names:
                id = self.org_name2id.get(name, None)
                if not id:
                    print 'not found ', name
                    continue
                count += self.org_count.get(id, 0)
            return count

        total_new, total = 0, 0
        for row in content[1:]:
            if row[0] == 'Total':
                row[14] = total_new
                row[15] = total
                break
            names = row[1].split('?')[1]
            names = names.split('&')
            names = [name.split('=')[1].strip() for name in names]
            count = get_count(names)
            row[14] = count - int(row[15])
            row[15] = count
            total_new += row[14]
            total += count
        write_csv(outfname, content)


    def top100_csv(self, filename, outfname, lang='en'):
        content=[]
        with open(filename) as f:
            reader = csv.reader(f)
            for x in reader:
                content.append(x)
        self.content = content

        for row in content[1:]:
            if len(row) ==0:
                print 'error ', row
                continue
            id = row[0]
            rec = self.ds.get(id, None)
            if not rec:
                print id, ' deleted'
                rec_title, org_title = self.get_deleted_dataset(id)
                print (rec_title, org_title)
                rec_title = json.loads(rec_title)
                org_title = org_title.split('|')
                row[1] = rec_title['en'] if lang =='en' else rec_title['fr']
                row[2] = org_title[0] if lang =='en' else org_title[1]
                continue
            org_id = rec['owner_org']
            org = self.orgs.get(org_id, None)
            if not org:
                continue
            title = rec['title_translated']
            row[1] = title['en'] if lang =='en' else title['fr']
            row[2] = org[0] if lang =='en' else org[1]
        write_csv(outfname, content)

    def download(self):
        if not self.file:
            # dataset http://open.canada.ca/data/en/dataset/c4c5c7f1-bfa6-4ff6-b4a0-c164cb2060f7
            url='http://open.canada.ca/static/od-do-canada.jl.gz'
            r = requests.get(url, stream=True)

            f = tempfile.NamedTemporaryFile(delete=False)
            for chunk in r.iter_content(1024 * 64):
                    f.write(chunk)
            f.close()
            self.download_file = f.name

        records = []
        fname = self.file or f.name
        try:
            with gzip.open(fname, 'rb') as fd:
                for line in fd:
                    records.append(json.loads(line.decode('utf-8')))
                    if len(records) >= 5000:
                        yield (records)
                        records = []
            if len(records) >0:
                yield (records)
                records = []
        except GeneratorExit:
            pass
        except:
            import traceback
            traceback.print_exc()
            print('error reading downloaded file')
            sys.exit(0)


def main():
    ga = GA_Report(sys.argv[1])
    ga.process(*sys.argv[2:])

main()

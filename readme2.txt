git config --global http.proxy "http.."
git config --global https.proxy "..."
git config --unset http.proxy
git config --unset https.proxy

#.ssh/config (.proxyauth format user:passwd)
ProxyCommand /usr/bin/corkscrew xxx.yyy.ca 80 %h %p /home/user/.proxyauth
Host reg1
Hostname  xx.fulldomain.ca

sudo apt-get install lxc lxctl lxc-templates
sudo ls /usr/share/lxc/tempates
sudo lxc-create -t ubuntu -n ckan
sudo lxc-start -n ckan -d
sudo lxc-info -n ckan
sudo lxc-ls
sudo lxc-console -n clan (ctrl+a q)
sudo lxc-stop -n ckan
sudo lxc
sudo ssh -X ubuntu@....
#ubunt: sudo apt-get install xauth ; re-login

#install .deb file
sudo apt install gdebi
#open .deb file with gdebi (right click)

#add hostname into ssh hosts
	nano ~/.ssh/config
#paste as
Host reg1
Hostname < xxx.gc.ca full host name>

#show file permission in digits
find /home/jffan/.ssh/ -printf '%m %p\b'

#share history between term tabs
export PROMPT_COMMAND='history -a; history -r'

# postges shell commands
sudo -u postgres psql <db>
=# \x  -- expand result into '\G' in mySql
=# \dt -- show all tables
=# \d+ <table name> -- show table schema
psql -d dbname -t -A -F"," -c "select * from users" > output.csv

curl --noproxy 127.0.0.1 'http://127.0.0.1:8983/solr/ckan_registry/select?q=id:96d276ba-5c5d-4927-8e15-f9b7cd25f879' 
           -H 'Content-type:text/xml; charset=utf-8'

curl http://localhost:8080/solr/update?commit=true -H "Content-Type: text/xml" 
	--data-binary '<delete><query>id:12345</query></delete>'
curl --noproxy 127.0.0.1 'http://127.0.0.1:8983/solr/ckan_registry/update?commit=true' 
       --data-binary '<delete><id>96d276ba-5c5d-4927-8e15-f9b7cd25f879</id></delete>' -H 'Content-type:text/xml; charset=utf-8'
replace the above <id></id> with <query></query> if not work

curl 'http://server:7773/solr/src01EN/update?commit=true' -H "Content-Type: text/xml" --data-binary '<delete><query>(fetchedDate_dt:[* TO NOW/DAY-3DAYS])AND(ds:imdb)</query></delete>'  #query first!

http://localhost:8983/solr/my_core_name/update?commit=true&stream.body=<delete><query>*%3A*</query></delete>
http://ca:7773/solr/col_rich003/select?q=id:*www.statcan.gc.ca/eng/survey/agriculture/3401&wt=json&indent=true&fl=id
http://ca:7773/solr/col_rich003/update?commit=true&stream.body=<delete><query>id:*www.statcan.gc.ca/eng/survey/agriculture/3401</query></delete>
http://c.ca:7773/solr/src01EN/update?commit=true&stream.body=<delete><query>-_lw_data_source_s:*</query></delete>
http://c.ca:7773/solr/src01FR/update?commit=true&stream.body=<delete><query>-_lw_data_source_s:*</query></delete>

to re-add the id into solr index: 
  go back to original url for edit, i.e., http://127.0.0.1:5000/en/dataset/a5cfb4d9-3303-48c8-a41f-d78b289d3e50
  edit--> update.

ckan obj url withour solr: ..../url/dataset/xxxx?__no_cache__=True

#solr link for search sort, (sort field is different from return fl)
http://localhost:8983/solr/ckanregistry/select/?q=collection%3Aprimary&version=2.2&start=0&rows=10&indent=on&sort=title_fr+desc&fl=title+id+extras_title_translated

first stop the paster server; before restore
sudo -u postgres pg_restore --clean --if-exists -d ckanregistry --format=custom  ../ckanext-canada/ckan.dump
sudo -u postgres pg_restore --clean --if-exists -d ckanregistry  ../ckanext-canada/ckan.dump

#migrate organization from registry to portal
#rehat need (scl enable python27 bash) or see open-scripts
ckanapi dump organizations --all -r/-c ...-registry.ini | bin/transitional_orgs_filter.py > transitional_orgs.jsonl
ckanapi load organization -c ..-public.ini -I tran..jsonl

#migrate datasets from registry to portal
change the above organization to datasets

# user json list in ckanapi parameters
ckanapi -c /etc/ckan/default/development-registry.ini action package_search fl:'["id", "extras_title_translated", "extras_status"]' q=collection:fgp

#rebuild solr for package
paster search-index rebuild a34eb330-7136-4f5e-9f5f-3ba41df58b06 -c production.ini

#download csv for pd
paster recombinant target-datasets  -c /etc/ckan/default/development-registry.ini
paster recombinant combine -a  -c /etc/ckan/default/development-registry.ini

#check csv from portal
ckanapi action package_show id=wrongdoing -r http://f7odwebb4.stcpaz.statcan.gc.ca/vl

#rebuild solr for pd
paster contracts rebuild  -c /etc/ckan/default/development-public.ini -f ../ckanext-recombinant/csv/contracts.csv ../ckanext-recombinant/csv/contracts.csv

#copy dataset between two ckan instance
paster canada copy-datasets http://registry.open.canada.ca/en 270643e1-d03f-4580-8108-44923c34e5d1 -l /home/jffan/src/data/portal_update.1.log  -t 1 -d 5  -f -c /etc/ckan/default/development-public.ini

#ckan test case
nosetests ckan/tests/logic/action/test_get.py --ckan --with-pylons=/etc/ckan/default/development-test.ini --pdb-failures
# with -s we can see the stdout, such as using pdb.set_trace(); need to paster initdb on first config use

git diff | colordiff | less -r #keep color in less
git commit --amend OR git rebase -i SHA1~1
# to combine two commits into one in git
	git rebase --interactive HEAD~2
    git rebase -i --root master # upto init commit
#edit the 'pick' to 'squash' for the commits that you do not want to show

#search string through git history
git log <-p> -S<string> | grep 'string'

#show specific commit details
git show <commit id>

#git change remote url
git remote set-url <upstream or origin> <url>

#create separate pull request to upstream
	git checkout -b <your_feature_branch> upstream/master
	git cherry-pick <sha1_of_1st_commit for feature X> <sha1 of last commit of feature X>
	git push origin <you_feature_branch>
	... <create pull request>

#delete both remote and local branch
	git push origin :<your branch>
  which is equal to `git push origin --delete <your branch>
	git branch -d -r <your branch>
  option:	git remote prune origin

#git resolve conflict
git checkout --theirs/ours ...file.x

#git set/change remote branch tracking
git branch -u origin/my_branch <local branch>

#git show branch tracking
git branch -vv

#git show committed files name only
git log --name-only <--oneline>

#git show commits from one user only
git log -p --author="Xxxx"

#git combine all your changes to one
git reset upstream/master
git add --all
git commit -a
git push -f origin master

#work with git clone bare
git clone --bare http://xxx
#show bare file
git log --pretty=format: --name-only --diff-filter=A | sort -  
#checkout from local bare clone; push into local bare,
git clone ~/src/xxx
git push ~/src/xxx master
git checkout master
git pull  #from local clone
git status -n xxx

#bare remote repo
@remote host #mkdir /tmp/testgit; cd testgit; git init --bare
@local #git remote add rbare ssh://u@rhost/tmp/testgit; git push -f rbare master
@remote working dir #git clone /tmp/testgit; git pull 

#skip proxycommand host 'Host !localost'

#get github commit diff
http://github.com/xxx/yyy/commmit/${SHA1}.patch

#git try a pull request PR
git pull upstream pull/2444/head

#git reset
git reset --hard

#git show tags
git log --tags --show-notes

#git generate patch from commit

git format-patch -1 <sha1>/HEAD

#sync time with http
sudo date -s "$(wget --no-cache -S -O /dev/null google.com 2>&1 | \
sed -n -e '/  *Date: */ {' -e s///p -e q -e '}')"

#maintain dev ckan environment
1. pip install git+http...ckanext-canada.git#egg... (pre condition once if not installed)
2.a under recombinant: paster recombinant create -a -c /etc/ckan/default/development-registry.ini
	will update or create new datastore table for PD
2.b git pull inside all the extensions from time to time

3. if its ckan master:
need another python virtual environment, another solor instance, and conf file

#find files exclude directories
find ./ -name '*.js' -not -path '*.mini.js' -not -path '*bootstrap*'  -not -path '*slick*'  -exec grep -Hin 'icon-' {} \; |less

#time bash stderr redirect
(time xxx) > file.log 2>&1
#localhost test ckan
use localhost instead of 127.0.0.1:5000 when using autocomplete etc

#local edit markdown file
sudo apt install ruby-dev libicu-dev cmake
sudo gem install gollum
sudo gem update gollum-lib
sudo apt intall ruby-github-markdown
#run gollum; ???.md need to be in git control
gollum


#use gui clipboard
sudo apt install glipper
glipper

#reinstall python package
pip list | grep xxx
pip uninstall xxx
  -- goto dir
pip install -e ./
gunicorn restart (not reload)


#custom install pip3 module
wget  --no-check-certificate     https://bootstrap.pypa.io/get-pip.py
python3 -m pip   install lmdb -t ./Lib --global-option=build_ext --global-option="-I/srv/python/latest/Include/:/srv/python/latest/" 

#OBD dev
# sudo systemctl restart uwsgi_portal

#ckan failed mail
/var/spool/mqueue

ps -eo pid,ppid,lstart,cmd
gcc -E a.c | grep off_t  #show all included files
stat -c "%a %n" filename #show file permission as number

#jsonl <--> json
 ckanapi dump datasets  59a56d3b-2b71-43aa-8ea2-6bc83c16b911 -r http://registry.open.canada.ca > /tmp/a.jsonl
cat /tmp/a.jsonl | python -m json.tool > /tmp/b.out
 ... now edit b/out ...
tr -d '\n' < /tmp/b.out  > /tmp/c.out
ckanapi load datasets -I /tmp/c.out -r http://registry.open.canada.ca -a "xx-xx-xx-xx"

#bash clear history
cat /dev/null > ~/.bash_history && history -c && exit

#use no proxy in a system wide proxy_set environment
1. Chrome: google-chrome --no-proxy-server
2. ssh: ssh -F <empty file> user@host

#use no_proxy with paster and ckanapi
no_proxy=127.0.0.1,10.0.2.15 ckanapi/paster

#intall python venv from non system py version
#midir tmp; cd tmp
#curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
#export PATH=$PATH:<your python>
#python ./get-pip.py -t ./ --upgrade
#python pip install virtualenv -t ./ --upgrade
#python3 -m venv <you venv directory> ; for v2.7, virtualenv --python=<your python bin> <your venv>
                    # for py2, need to ./configure --prefix=<src>; make; make install; point to the bin/python2


#convert html to markdown
#pandoc test.html -o test.md

#id
#sudo mount -t vboxsf  -o uid=1000 ubuntuShare host
pkexec visudo  #user ALL:(ALL) NOPASSWD:ALL

#build pcl
1. sudo apt install libproj-dev
2. add the following line to your CMakeLists.txt file: 
     list(REMOVE_ITEM PCL_LIBRARIES "vtkproj4")

#crontab
#* * * * * touch /tmp/test-$(date +\%Y\%m\%d).log

#restart network
#sudo service network-manager restart

#build openssl
wget openssl.org/..tgz&& tar
./config --prefix=/home/usr/openssl --openssldir=/home/usr/openssl enable-weak-ssl-ciphers
make && make install
export LD_LIBRARY_PATH=/home/usr/openssl/lib

#lock file in shell
LOCKFILE=/tmp/lock.txt
if [ -e ${LOCKFILE} ] && kill -0 `cat ${LOCKFILE}`; then
    echo "already running"
    exit
fi
# make sure the lockfile is removed when we exit and then claim it
trap "rm -f ${LOCKFILE}; exit" INT TERM EXIT
echo $$ > ${LOCKFILE}
# do stuff
sleep 1000
rm -f ${LOCKFILE}

#unicode \u1234 in python3 hex(ord('ç')) = 0xe7 = \u00e7


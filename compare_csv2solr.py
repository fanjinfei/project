#!/usr/bin/env python

import json
import os, sys
from lxml import etree
import csv
import requests
import time
'''
curl -o portal_datasets.out --request POST 'http://solr.host/core_staging_portal/select/' 
  --data 'q=dataset_type%3Adataset&version=2.2&start=0&rows=118000&indent=on&fl=id'
'''
def read_solrXml(filename):
    root = etree.parse(filename).getroot()
    i = 0
    solr_refs = []
    for element in root.iter("doc"):
        i += 1
        if i > 2 and False:
            break
        #print (etree.tostring(element))
        for l in element.iter("str"):
            if l.attrib['name']=="id":
                solr_refs.append(l.text)
    return solr_refs

'''
 psql -U <user> -d <db name> -h db.host -t -A -F","
   -c "select id from package where state='active' and type='dataset'" > ~/tmp/opendata_datasets_4.csv
'''
def read_csv(filename):
    content=[]
    with open(filename) as f:
        reader = csv.reader(f)
        for x in reader:
            if x:
                content.append(x[0].strip())
    return content

def write_csv(filename, ids):
    with open(filename, 'wb') as f:
        writer = csv.writer(f)
        for x in ids:
            writer.writerow([x])

def diffIds(solrIds, csvIds):
    return list (set(csvIds) - set(solrIds))

def getRecordDetails(id):
    url='http://registry.open.canada.ca/api/action/package_show?id='
    url += id
    retry = 5
    while True:
        try:
            r = requests.get(url, timeout=30)
            break
        except (requests.exceptions.ReadTimeout, requests.exceptions.Timeout):
            retry -= 1
            if retry <=0:
                print(id, 'timeout')
                return (id, None, None, None)
            time.sleep(5)
    try:
        res = r.json()
        res = res['result']
        return (id, res['ready_to_publish'], res['imso_approval'], res['metadata_modified'])
    except:
        print(id, 'query error')
        return (id, None, None, None)
    
def main():
    print (sys.argv)
    sids = read_solrXml(sys.argv[1])
    cids = read_csv(sys.argv[2])
    print(len(sids), sids[:5])
    print(len(cids), cids[:5])

    ldiff = diffIds(sids, cids)
    print (len(ldiff), ldiff[:5])
    
    #write_csv("/tmp/diff.csv", ldiff)
    res = []
    for id in ldiff:
        res.append( getRecordDetails(id) )
    for r in res:
        if r[1]=='true' and r[2]=='true':
            print(r)
main()

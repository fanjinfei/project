"""Hello Analytics Reporting API V4."""

import argparse

from apiclient.discovery import build
import httplib2
from oauth2client import client
from oauth2client import file
from oauth2client import tools

import os
import urllib
import sys

proxy= os.environ['http_proxy']

# https://developers.google.com/analytics/devguides/reporting/core/v4/quickstart/installed-py
# https://developers.google.com/analytics/devguides/reporting/core/v4/basics

SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
DISCOVERY_URI = ('https://analyticsreporting.googleapis.com/$discovery/rest')
#CLIENT_SECRETS_PATH = 'client_secrets.json' # Path to client_secrets.json file.
#VIEW_ID = '<REPLACE_WITH_VIEW_ID>'
CLIENT_SECRETS_PATH = sys.argv[1]
VIEW_ID = sys.argv[2]


def initialize_analyticsreporting():
  """Initializes the analyticsreporting service object.

  Returns:
    analytics an authorized analyticsreporting service object.
  """
  # Parse command-line arguments.
  parser = argparse.ArgumentParser(
      formatter_class=argparse.RawDescriptionHelpFormatter,
      parents=[tools.argparser])
  # flags = parser.parse_args(['--noauth_local_webserver'])
  flags = parser.parse_args([])

  # Set up a Flow object to be used if we need to authenticate.
  flow = client.flow_from_clientsecrets(
      CLIENT_SECRETS_PATH, scope=SCOPES,
      message=tools.message_if_missing(CLIENT_SECRETS_PATH))

  # Prepare credentials, and authorize HTTP object with them.
  # If the credentials don't exist or are invalid run through the native client
  # flow. The Storage object will ensure that if successful the good
  # credentials will get written back to a file.
  storage = file.Storage('analyticsreporting.dat')
  credentials = storage.get()

  pi = httplib2.proxy_info_from_environment('http')
  if credentials is None or credentials.invalid:
    credentials = tools.run_flow(flow, storage, flags, http=httplib2.Http(proxy_info=pi))

  http = credentials.authorize(http=httplib2.Http(proxy_info=pi))

  # Build the service object.
  analytics = build('analytics', 'v4', http=http, discoveryServiceUrl=DISCOVERY_URI)

  return analytics

def get_report_sample(analytics):
  # Use the Analytics Service Object to query the Analytics Reporting API V4.
  return analytics.reports().batchGet(
      body={
        'reportRequests': [
        {
          'viewId': VIEW_ID,
          'dateRanges': [{'startDate': '17daysAgo', 'endDate': 'today'}],
          'metrics': [{'expression': 'ga:sessions'}]
        }]
      }
  ).execute()

def get_report(analytics, filename, start, end):
  # Use the Analytics Service Object to query the Analytics Reporting API V4.
  return analytics.reports().batchGet(
      body={
        'reportRequests': [
        {
          'viewId': VIEW_ID,
          'dateRanges': [{'startDate': start, 'endDate': end}],
          'metrics': [ 
                      {'expression': 'ga:totalEvents'},
                      {'expression': 'ga:eventValue'},
                      {'expression': 'ga:uniqueEvents'}                       
                     ],
          'dimensions':[{'name':'ga:eventLabel'}],
#          'dimensions':[{'name':'ga:browser'}],
#          'metricFilterClauses': [ {
#                'filters': [{
#                    'metricName': 'ga:pagePath',
#                    'operator': "CONTAIN",
#                    'comparisonValue': '/data/en/dataset/'
#                    }]
#                }]

          'dimensionFilterClauses': [ {
                'operator': 'AND',
                'filters': [{
                    'dimensionName': 'ga:eventLabel',
                    'operator': "PARTIAL",
#                    'expressions': ['dd-publications.xml']
                    'expressions': ['pub-pub%2F'+filename]
                    
                    },{
                                'dimensionName': 'ga:eventAction',
                                'operator': "PARTIAL",
                                'expressions': ['download']
                        }],
                }],

#          'dimensionFilterClauses': [ {
#                'operator': 'AND',
#                'filters': [{
#                    'dimensionName': 'ga:pagePath',
#                    'operator': "BEGINS_WITH",
#                    'expressions': ['/data/en/dataset?']
#                    },{
#                     'dimensionName': 'ga:pagePath',
#                     'operator': "PARTIAL",
#                     'expressions': ['fgp']
#                    }]
#                }],

          'orderBys':[
            {'fieldName': 'ga:totalEvents',
             'sortOrder': 'DESCENDING'
            }],
           'pageToken': '0',
           'pageSize': '100',
            
        }]
      }
  ).execute()

def get_report_region(analytics):
  # Use the Analytics Service Object to query the Analytics Reporting API V4.
  return analytics.reports().batchGet(
      body={
        'reportRequests': [
        {
          'viewId': VIEW_ID,
          'dateRanges': [{'startDate': '7daysAgo', 'endDate': 'today'}],
          'metrics': [{'expression': 'ga:sessions'},
                      {'expression': 'ga:pageviews'} ],
          'dimensions':[{'name':'ga:region'}],
#          'dimensions':[{'name':'ga:browser'}],
#          'metricFilterClauses': [ {
#                'filters': [{
#                    'metricName': 'ga:pagePath',
#                    'operator': "CONTAIN",
#                    'comparisonValue': '/data/en/dataset/'
#                    }]
#                }]

          'dimensionFilterClauses': [ {
                'operator': 'AND',
                'filters': [{
                    'dimensionName': 'ga:pagePath',
                    'operator': "BEGINS_WITH",
                    'expressions': ['/data/en/dataset?']
                    },{
                     'dimensionName': 'ga:pagePath',
                     'operator': "PARTIAL",
                     'expressions': ['fgp']
                    },{
                     'dimensionName': 'ga:pagePath',
                     'operator': "PARTIAL",
                     'expressions': ['collection']
                    }]
                }, {
                    'filters': [{
                    'dimensionName': 'ga:country',
                    'operator': "BEGINS_WITH",
                    'expressions': ['Canada']
                    }]
                }],
#          'dimensionFilterClauses': [ {
#                'operator': 'AND',
#                'filters': [{
#                    'dimensionName': 'ga:pagePath',
#                    'operator': "BEGINS_WITH",
#                    'expressions': ['/data/en/dataset?']
#                    },{
#                     'dimensionName': 'ga:pagePath',
#                     'operator': "PARTIAL",
#                     'expressions': ['fgp']
#                    }]
#                }],

          'orderBys':[
            {'fieldName': 'ga:pageviews',
             'sortOrder': 'DESCENDING'
            }],
           'pageToken': '0',
           'pageSize': '100',
            
        }]
      }
  ).execute()


def print_response(response):
  """Parses and prints the Analytics Reporting API V4 response"""

  for report in response.get('reports', []):
    columnHeader = report.get('columnHeader', {})
    dimensionHeaders = columnHeader.get('dimensions', [])
    metricHeaders = columnHeader.get('metricHeader', {}).get('metricHeaderEntries', [])
    rows = report.get('data', {}).get('rows', [])

    for row in rows:
      dimensions = row.get('dimensions', [])
      dateRangeValues = row.get('metrics', [])

      for header, dimension in zip(dimensionHeaders, dimensions):
        print header + ': ' + dimension

      for i, values in enumerate(dateRangeValues):
        print 'Date range (' + str(i) + ')'
        for metricHeader, value in zip(metricHeaders, values.get('values')):
          print metricHeader.get('name') + ': ' + value


def main():

  analytics = initialize_analyticsreporting()
  dates = [ '2014-01-01 2014-12-31', '2015-01-01 2015-12-31',
             '2016-01-01 2016-12-31', '2017-01-01 today', '2012-01-01 today']
  names = [ 'dd-publications.xml', 'monographies-monographs.zip',
        'periodiques-periodicals.zip', 'series.zip']
  for s in names[3:]:
    print s  
    for d in dates:
        [start, end] = d.split(' ')
        print start, end
        response = get_report(analytics, s, start, end)
        #response = get_report_region(analytics)
        print_response(response)

if __name__ == '__main__':
  main()

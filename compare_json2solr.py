#!/usr/bin/env python

import json
import os, sys
from lxml import etree
import csv
import requests
import time
'''
curl -o portal_datasets.out --request POST 'http://solr.host/core_staging_portal/select/' 
  --data 'q=dataset_type%3Adataset&version=2.2&start=0&rows=118000&indent=on&fl=id'

curl -o ~/src/data/portal2-cansim2.xml 'http://f7oddatab1.statcan.gc.ca:8080/core_staging_portal/select/?q=extras_data_series_name%3A"Summary+tables"&version=2.2&start=0&rows=8000&indent=on&fl=id+validated_data_dict'

 curl -o ~/src/data/portal2-cansim1.xml 'http://f7oddatab1.statcan.gc.ca:8080/core_staging_portal/select/?q=extras_data_series_name%3ACANSIM&version=2.2&start=0&rows=8000&indent=on&fl=id+validated_data_dict'
'''
def read_solrXml(filename):
    root = etree.parse(filename).getroot()
    i = 0
    solr_refs = []
    for element in root.iter("doc"):
        i += 1
        if i > 2 and False:
            break
        #print (etree.tostring(element))
        for l in element.iter("str"):
            if l.attrib['name']=="id":
                solr_refs.append(l.text)
    return solr_refs

'''
 psql -U <user> -d <db name> -h db.host -t -A -F","
   -c "select id from package where state='active' and type='dataset'" > ~/tmp/opendata_datasets_4.csv
'''
def read_json(filename):
    content=[]
    with open(filename) as f:
        for line in f:
            rec = json.loads(line)
            content.append(rec["id"])
    return content

def write_csv(filename, ids):
    with open(filename, 'wb') as f:
        writer = csv.writer(f)
        for x in ids:
            writer.writerow([x])

def diffIds(solrIds, ids):
    return list (set(solrIds) - set(ids))

def getRecordDetails(id):
    url='http://open.canada.ca/data/api/action/package_show?id='
    url += id
    retry = 5
    while True:
        try:
            r = requests.get(url, timeout=30)
            break
        except (requests.exceptions.ReadTimeout, requests.exceptions.Timeout):
            retry -= 1
            if retry <=0:
                print(id, 'timeout')
                return (id, None, None, None)
            time.sleep(5)
    try:
        res = r.json()
        res = res['result']
        return (id, res['owner_org'])
    except:
        print(id, 'query error')
        return (id, None)
    
def main():
    print (sys.argv)
    sids = read_solrXml(sys.argv[1])
    cids = read_json(sys.argv[2])
    print(len(sids), sids[:5])
    print(len(cids), cids[:5])

    ldiff = diffIds(sids, cids)
    print (len(ldiff), ldiff[:5])
    
#    return
    
    res = []
    for id in ldiff:
        res.append( getRecordDetails(id) )
    for r in res:
        print(r)
        if r[1].upper() !='A0F0FCFC-BC3B-4696-8B6D-E7E411D55BAC':
            ldiff.remove(r[0])
    write_csv("/tmp/diff.csv", ldiff)
main()

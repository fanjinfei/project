#How to install TinyVisor  

###This page explains the procedure to install TinyVisor, create one VM, and start the OS. Installation is done on Linux (x86_64).

###TinyVisor can be installed in both BIOS environment and UEFI environment.

The flow is as follows.

    Build the binary  
    install  

There are three ways to install 2.

    Installing to USB memory (UEFI)  
    Installing to USB memory (BIOS)  
    Install on Linux file system (BIOS)  


##1. Build the binary
###1.1 Installing packages required for build

Please install on the build machine as the next package is necessary (there may be other).

    Gcc - The GNU C compiler  
    Iasl - Intel ASL compiler / decompiler (or acpica - tools)  
    Make - An utility for Directing compilation.  

For the UEFI environment, please also install the following package.

    Mingw - w64 - Development environment targetting 32 - and 64 - bit Windows (or mingw64 - gcc)

###1.2 Acquire the source tree

Download the archive of the source tree from the next page.

    Download file list

After downloading, expand the source tree and move to the generated directory.

Tar zxvf tinyvisor - *. Tar.gz  
Cd tinyvisor *

The source tree after 1.7 is managed by Mercurial. To get the source code directly from Mercurial's repository, execute the following command:

Mkdir tinyvisor  
Cd tinyvisor  
Hg clone http://hg.osdn.jp/view/tinyvisor/tinyvisor-vmm vmm  
Hg clone http://hg.osdn.jp/view/tinyvisor/tinyvisor-bios bios  

Source trees before 1.6 are managed by Subversion. To get the source code directly from the Subversion repository, execute the following command:

Svn checkout http://svn.osdn.jp/svnroot/tinyvisor/trunk tinyvisor  
Cd tinyvisor

###1.3 Build

As of 1.7, build with the following command.

Make -C vmm -j 8  
Make -C vmm / boot / loader  
Make -C vmm / boot / uefi-loader  
LC_ALL = C make -C Bios -j 8  

Before 1.6, build with the following command.

Make


##2-A. Install to USB memory (UEFI)

The procedure when USB memory is / dev / sdf is described. If different, please read over.  
When installing for the first time
###2a.1 Delete an existing partition

Sudo / sbin / sgdisk - Z / dev / sdf  
GPT data structures destroyed! You may now partition the disk using fdisk or
Other utilities.

###2a.2 Create a partition with code = ef00 (EFI System) in GPT

Sudo / sbin / gdisk / dev / sdf  
GPT fdisk (gdisk) version 0.8.5  

Partition table scan:  
  MBR: not present  
  BSD: not present  
  APM: not present  
  GPT: not present  

Creating new GPT entries.

Command (? For help): n  
Partition number (1-128, default 1):  
First sector (34-492510, default = 2048) or {+ -} size {KMGTP}:  
Last sector (2048-492510, default = 492510) or {+ -} size {KMGTP}:  
Current type is 'Linux filesystem'  
Hex code or GUID (L to show codes, Enter = 8300): ef 00  
Changed type of partition to 'EFI System'  

Command (? For help): p  
Disk / dev / sdf: 492544 sectors, 240.5 MiB  
Logical sector size: 512 bytes  
Disk identifier (GUID): 44D93EB8-59E3-4222-B8CF-089083DC5D67  
Partition table holds up to 128 entries  
First usable sector is 34, last usable sector is 492510  
Partitions will be aligned on 2048-sector boundaries  
Total free space is 2014 sectors (1007.0 KiB)  

Number Start (sector) End (sector) Size Code Name  
   1 2048 4925 10 239.5 MiB EF 00 EFI System  

Command (? For help): w  

Final checks complete. About to write GPT data. THIS WILL OVERWRITE EXISTING  
PARTITIONS !!  

Do you want to proceed? (Y / N): y  
OK; writing new GUID partition table (GPT) to / dev / sdf.  
The operation has completed successfully.  

###2a.3 Format the created partition with FAT32

```
Sudo /sbin/mkfs.vfat - F 32 / dev / sdf 1  
Mkfs.vfat 3.0.13 (30 Jun 2012)
```

###2a.4 Install binaries to FAT32 partitions

For 1.7 or later, execute the following command.

```Sudo vmm / boot / uefi - loader / install.sh - f / dev / sdf 1```

For 1.6 or earlier, execute the following command.

```Sudo ./install_to_usb.sh - e - f / dev / sdf 1```

###2a.5 Start VMM

Insert the USB memory and turn on the PC.

In the BIOS menu, if the priorities of the HDDs on which the OS is installed are set lower for the priority of the USB memory, increase the priority order of the USB memory and restart the PC again.

When VMM is loaded from the USB memory and start up, a message is output to the screen and COM1. When the start of VMM is completed, VM 0 starts to run and it returns to the UEFI of the Host. The OS is loaded from the HDD of the next priority of the USB memory and it starts up.  
When installing after the second time
####1. Install binaries to FAT32 partitions

For 1.7 or later, execute the following command.

```Sudo vmm / boot / uefi-loader / install.sh / dev / sdf 1```

For 1.6 or earlier, execute the following command.

```Sudo ./install_to_usb.sh - e / dev / sdf 1```
Tuesday, October 7, 2014 23: 19: 40 JST

####2. Start VMM

Insert the USB memory and turn on the PC.

In the BIOS menu, if the priorities of the HDDs on which the OS is installed are set lower for the priority of the USB memory, increase the priority order of the USB memory and restart the PC again.

When VMM is loaded from the USB memory and start up, a message is output to the screen and COM1. When the start of VMM is completed, VM 0 starts to run and it returns to the UEFI of the Host. The OS is loaded from the HDD of the next priority of the USB memory and it starts up.


##2 - B. Installing to USB memory (BIOS)

The procedure when USB memory is / dev / sdf is described. If different, please read over.  
When installing for the first time  
###2b.1 Delete an existing partition


Sudo / sbin / sgdisk - Z / dev / sdf  
GPT data structures destroyed! You may now partition the disk using fdisk or
Other utilities.  

###2b.2 Create partition on USB memory


Create a single partition using the entire USB memory.

```Sudo / sbin / fdisk / dev / sdf```

Welcome to fdisk (util-linux 2.25.2).  
Changes will remain in memory only until you decide to write them.  
Be careful before using the write command.  

Device does not contain a recognized partition table.  
Created a new DOS disklabel with disk identifier 0x6a 01f 56c.

Command (m for help): n  
Partition type  
   P primary (0 primary, 0 extended, 4 free)  
   E extended (container for logical partitions)  
Select (default p):  

Using default response p.  
Partition number (1-4, default 1):  
First sector (2048 - 1965055, default 2048):  
Last sector, + sectors or + size {K, M, G, T, P} (2048 - 1965055, default 1965055):  

Created a new partition 1 of type 'Linux' and of size 958.5 MiB.

Command (m for help): p  
Disk / dev / sdf: 959.5 MiB, 1006108672 bytes, 1965056 sectors  
Units: sectors of 1 * 512 = 512 bytes  
Sector size (logical/physical): 512 bytes / 512 bytes  
I/O size (minimum/optimal): 512 bytes / 512 bytes  
Disklabel type: dos  
Disk identifier: 0x6a01f56c  

Device     Boot Start     End Sectors   Size Id Type  
/dev/sdf1        2048 1965055 1963008 958.5M 83 Linux  


Command (m for help): w  
The partition table has been altered.  
Calling ioctl() to re-read partition table.  
Syncing disks.  


###2b.3 Install binary in USB memory

For 1.7 or later, execute the following command.

```Sudo vmm / boot / loader / install.sh -f -c 'vm0.boot_int18' / dev / sdf 0 1 vmm / boot / loader / bootloader vmm / vmm.elf```

For 1.6 or earlier, execute the following command.

```Sudo ./install_to_usb.sh -f -c 'vm0.boot_int18' / dev / sdf```

"Vm0.boot_int18" is a parameter that instructs to start the OS of VM0 from the HDD of the next boot order of the USB memory in which VMM is stored. If you do not specify this parameter, VMM will try again to panic.

###2b.4 Start VMM

Insert the USB memory and turn on the PC.

In the BIOS menu, if the priorities of the HDDs on which the OS is installed are set lower for the priority of the USB memory, increase the priority order of the USB memory and restart the PC again.

When VMM is loaded from the USB memory and start up, a message is output to the screen and COM1. When VMM startup is completed, VM0 starts to run and INT18 BIOS CALL of the Host BIOS is called. The OS is loaded from the HDD of the next priority of the USB memory and it starts up.

Depending on the BIOS, an error message saying that the OS can not be found is output, which may cause the startup of the OS to fail. In that case, please install "vm0.boot_drive = 81" instead of "vm0.boot_int18" and install it to USB memory. This parameter is a parameter instructing to boot the OS from the HDD of drive number 81 of the BIOS.

When installing after the second time  
####1. Install binary in USB memory

For 1.7 or later, execute the following command.

```Sudo vmm / boot / loader / install.sh -c 'vm0.boot_int18' / dev / sdf 0 1 vmm / boot / loader / bootloader vmm / vmm.elf```

For 1.6 or earlier, execute the following command.

```Sudo ./install_to_usb.sh -c 'vm0.boot_int18' / dev / sdf```

"Vm0.boot_int18" is a parameter that instructs to start the OS of VM0 from the HDD of the next boot order of the USB memory in which VMM is stored. If you do not specify this parameter, VMM will try again to panic.
####2 Start VMM

Insert the USB memory and turn on the PC.

In the BIOS menu, if the priorities of the HDDs on which the OS is installed are set lower for the priority of the USB memory, increase the priority order of the USB memory and restart the PC again.

When VMM is loaded from the USB memory and start up, a message is output to the screen and COM1. When VMM startup is completed, VM0 starts to run and INT18 BIOS CALL of the Host BIOS is called. The OS is loaded from the HDD of the next priority of the USB memory and it starts up.

Depending on the BIOS, an error message saying that the OS can not be found is output, which may cause the startup of the OS to fail. In that case, please install "vm0.boot_drive = 81" instead of "vm0.boot_int18" and install it to USB memory. This parameter is a parameter instructing to boot the OS from the HDD of drive number 81 of the BIOS.

##2 - C. Install on Linux file system (BIOS)
###2c.1 Copy the binary to / boot

Copy the binary to / boot. The following is an example when the build machine and the target machine are identical. If the build machine and the target machine are different, please copy to the appropriate directory on the target machine with ftp, scp etc, then move to the / boot directory.


Sudo cp vmm / vmm.elf / boot  
Sudo bios / out / bios.bin / boot  


Add the following entry to /etc/grub.d/40_custom. This entry creates only one VM.


Menuentry 'VMM (VM 0)' --class vmm {  
        Insmod part_msdos  
        Insmod ext 2  
        Set root = '(hd 0, msdos 1)'  
        Echo 'Loading VMM ...'  
        Multiboot / boot / vmm.elf  
}  


For Debian, execute the update-grub command. For Fedora, execute the grub2-mkconfig -o / boot / grub2 / grub.cfg command.
###2c.2 Start VMM

Restart the PC.

When the GRUB menu is displayed, select "VMM (VM 0)".

VMM startup is started and a message is output to the screen and COM1. When VMM startup is completed, VM0 starts running and the GRUB menu is displayed again. In this case, select OS rather than VMM and start OS.

For instructions on starting multiple OSs, see How to use TinyVisor.

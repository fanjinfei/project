# encoding: utf-8

import tarfile
import os
import sys
import glob
import time
from datetime import datetime
import csv
import unicodecsv
from unicodecsv import DictReader
import codecs
from collections import defaultdict
import traceback
import pdb

def read_csv(f): #unicode csv
#    with open(fn, 'rb') as f:
        import string
        string.whitespace = ('\t\n\x0b\x0c\r ')
        
        first3bytes = f.read(3)
        if first3bytes != codecs.BOM_UTF8:
            f.seek(0)

        csv_in = DictReader(f)
        cols = csv_in.unicode_fieldnames
        
        res = {}
        for row_dict in csv_in:
            a = row_dict['ref_number']
            b = a.strip()
            #if a !=b:
            #    print (a,b)
            res[row_dict['owner_org'] + "|" + b] = row_dict
        return res

def write_csv(filename, rows, header=None):
    outf=open(filename, 'wb')
    outf.write(codecs.BOM_UTF8)
    writer = unicodecsv.writer(outf)

    if header:
        writer.writerow(header)
    for row in rows:
        writer.writerow(row)

class Dataset:
    def __init__(self):
        pass
    def read_directory(self, subdir):
        self.files = glob.glob(subdir + '/2016/pd-????????.tar.gz') + \
                    glob.glob(subdir + '/2017/pd-????????.tar.gz')
#        self.files = glob.glob(subdir + '/2017/pd-2017090?.tar.gz') 
        self.files.sort()
        print self.files[:5]
    def process(self):
        res = {}
        for fn in self.files:
            dt = fn[-15:-7]
            print 'processing ' + fn + ' ' + dt
            records = self.read_file(fn)
            for ref, detail in records.iteritems():
                v = res.get(ref)
                if v:
                    if v[2] != detail:
                        v[1] = dt
                        v[2] = detail
                else:
                    res[ref] = [dt, dt, detail]
        rows = []
        rows.append(['owner_org|ref_number', 'upload date', 'modified date'])
        for k, v in res.iteritems():
            rows.append([k, v[0], v[1]])
        write_csv('/tmp/inventory_report.csv', rows)
        
    def read_file(self, fn):
        tar = tarfile.open(fn)
        try:
            f = tar.extractfile("inventory.csv")
            return read_csv(f)
        except KeyError:
            return {}

def main():
    ds = Dataset()
    ds.read_directory(sys.argv[1])
    ds.process()
      
if __name__ == '__main__':
  main()

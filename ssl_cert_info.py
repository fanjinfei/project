from OpenSSL import SSL
import socket
import sys
import re
import openpyxl
import traceback as tb
import multiprocessing
from multiprocessing import Process, Queue
import time

def verify_cb(conn, cert, errun, depth, ok):
        return True

def _get_ou(cert):
    ds = str(cert.get_issuer()).split('/')
    for d in ds:
        if d[:3] == 'OU=': return d[3:]
    return None

class MSSL():
    def __init__(self, PROXY_ADDR):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect(PROXY_ADDR)
        #self.s.settimeout(5)

    def get_cert_ou(self, server, port):
        CONNECT = "CONNECT %s:%s HTTP/1.0\r\nConnection: close\r\n\r\n" % (server, port)
        self.s.send(CONNECT)
        try:
            self.s.recv(4096)
        except socket.timeout:
            print 'timeout'
            return None

        ctx = SSL.Context(SSL.SSLv23_METHOD)
        ctx.set_verify(SSL.VERIFY_PEER, verify_cb)
        ss = SSL.Connection(ctx, self.s)

        try:
            ss.set_connect_state()
            print('pre-h')
            ss.do_handshake()
            print('post-h')
            cert = ss.get_peer_certificate()
            ou = _get_ou(cert)
            ss.shutdown()
            ss.close()
        except:
            #tb.print_exc()
            ou = None
        return ou

def mp(h, p, q):
    PROXY_ADDR = ("localhost", 8081)
    ms = MSSL(PROXY_ADDR)
    ou = ms.get_cert_ou(h, p)
    q.put(ou)
    return ou
    
class Data():
    def __init__(self, fname):
        self.book = openpyxl.load_workbook(fname)
        self.ws = self.book.get_sheet_by_name("Sheet1")

    def iterate_certs(self):
        irow = 0
        PROXY_ADDR = ("localhost", 8081)
        for row in self.ws.iter_rows():
            irow += 1
            if irow == 1: continue
            col = 0 #1
            if row[col].internal_value:
                #print row[1].internal_value
                #ws.cell(column=10, row=irow, value="what")
                hosts = self.get_hosts(row[col].internal_value)
                ports = self.get_ports(row[col+2].internal_value)
                ous = []
                for h in hosts:
                    for p in ports:
                        #ms = MSSL(PROXY_ADDR)
                        #ou = ms.get_cert_ou(h, p)
                        #print h, p; continue
                        res = Queue()
                        self.get_ou(h,p, res)  
                        #print h, p, (res.get() if not res.empty() else None)
                        if not res.empty():
                            r = res.get()
                            if r: ous.append(':'.join([h,str(p),r]))
                self.ws.cell(column=10, row=irow, value="\n".join(ous))
        self.book.save("/tmp/a.xlsx")
    def get_ou(self, h, p, res):
        p1 = multiprocessing.Process(target=mp, args=(h,p, res))
        p1.start()
        p1.join(5)
        if p1.is_alive():
           p1.terminate()
           p1.join()
    def get_hosts(self, v):
        i = v.find('https://')
        j = v.find('http://')
        if v.find('All Vlans') >=0 : return [ ]
        host = None
        if i >=0 : host = v[i+8:].strip()
        if j >=0 : host = v[j+7:].strip()
        if host: return [ host.rstrip('/') ]
        hosts =[ x.strip() for x in re.split('[\r\n]', v) ]
        fhosts  = []
        for x in hosts:
            if x.find('.gc.ca') >=0:
                fhosts.append(x)
            else:
                fhosts.append(x+'.stcpaz.statcan.gc.ca')
        return fhosts

    def get_ports(self, v):
        ports = [x.strip() for x in re.split('[\r\n,]', str(v))]
        fports = []
        for x in ports:
            if not x: continue
            i = x.find('(')
            if i >=0: x = x[:i]
            xs = x.split('-')
            if len(xs)==2:
                for p in range(int(xs[0]), int(xs[1])+1):
                    fports.append(p)
            else:
                if not x.isdigit(): continue
                fports.append(int(x))
        return fports
                
def main():
    d = Data(sys.argv[1])
    d.iterate_certs()

main()


'''
server = 'mail.google.com'
port = 443
PROXY_ADDR = ("localhost", 8081)
CONNECT = "CONNECT %s:%s HTTP/1.0\r\nConnection: close\r\n\r\n" % (server, port)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(PROXY_ADDR)
s.send(CONNECT)
print s.recv(4096)      

ctx = SSL.Context(SSL.SSLv23_METHOD)
ctx.set_verify(SSL.VERIFY_PEER, verify_cb)
ss = SSL.Connection(ctx, s)

ss.set_connect_state()
ss.do_handshake()
cert = ss.get_peer_certificate()
print cert.get_subject()
print cert
ss.shutdown()
ss.close()
'''

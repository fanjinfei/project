#windows putty usage
1. puttygen covert openssh private key to .ppk
2. putty session-->ssh auth --> load .ppk key file
3. pscp -load <putty session> <src file> <dest file>   such as [user@host:/home/xxx/]

#build tinycore from scratch

1. efiboot.img used in hybrid build ISO
  $ tce-load -i grub-efi dosfstools

  $ cd /tmp
  $ cat grub.cfg
   loadfont unicode
   insmod efi_gop
   set gfxterm_font=unicode
   terminal_output gfxterm

   menuentry "corepure64" {
     linux /boot/vmlinuz64 quiet
     initrd /boot/corepure64.gz
   }

  $ x86_64-grub-mkimage --format=x86_64-efi --output=BOOTX64.EFI --config=/tmp/grub.cfg --prefix=/EFI/BOOT part_gpt part_msdos fat ext2 hfs hfsplus iso9660 udf ufs1 ufs2 zfs chain linux boot appleldr ahci configfile normal regexp minicmd reboot halt search search_fs_file search_fs_uuid search_label gfxterm gfxmenu efi_gop efi_uga all_video loadbios gzio echo true probe loadenv bitmap_scale font cat help ls png jpeg tga test at_keyboard usb_keyboard

  $ dd if=/dev/zero of=efiboot.img bs=1K count=1440
  $ mkdosfs -F 12 efiboot.img
  $ mkdir /tmp/image
  $ sudo mount efiboot.img /tmp/image
  $ sudo mkdir -p /tmp/image/EFI/BOOT
  $ sudo cp BOOTX64.EFI /tmp/image/EFI/BOOT
  $ sudo umount /tmp/image

  [copy existing corepure64 iso files somewhere]
  $ cp efiboot.img /somewhere/boot/isolinux
   below see readme7.txt
  $ mkisofs -pad -l -r -J -V CorePure64 -no-emul-boot -boot-load-size 4 -boot-info-table -b boot/isolinux/isolinux.bin -c boot/isolinux/boot.cat -eltorito-alt-boot -e boot/isolinux/efiboot.img -boot-hide-rr-moved -o newCorePure64.iso somewhere

2. replace the above isolinux with grub2 used in BIOS boot
   grub-mkrescue -o ../grub.iso
   # copy from the above grub.iso a file named 'eltorito.img'
   $cp <eltorito.img> boot/grub/eltorito.img
   # replace '-b boot/isolinux/isolinux.bin' with '-b boot/grub/eltorito.img'

3. efibootmgr
  efibootmgr -v
  sudo efibootmgr -o 0002,0000,0001
  efibootmgr -c -d /dev/sda -p 7 -L <label> -l \\EFI\\<lable>\\grubx64.efi        //disk partition 7
  sudo efibootmgr -b <bootnum> -B                  // delete entry

4. clock used by linux
  # /sys/devices/system/clocksource/clocksource0/available_clocksource
  # cat /sys/devices/system/clocksource/clocksource0/current_clocksource


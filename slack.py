from slackclient import SlackClient
import sys, subprocess
import requests
import time

''' Service Monitor - monitor (Browse Apps/ Custom Integration/Incoming Webhooks/
https://hooks.slack.com/services/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
'''
def sendSlack2(text):
    url = 'https://hooks.slack.com/services/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'
    payload = {"text":text}
    count = 5
    while count >0:
        count -= 1
        r = requests.post(url, json=payload)
        if r.status_code == 200:
            break

def get_csv_upload(usr):
    cmd = 'ssh ' + usr + ' stat -c %Y /home/odatsrv/run_log/upload_ati_csv_from_datastore_tables.log'
    cmd = cmd.split(' ')
    result = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0]
    now = time.time()
    if now - int(result) > 3600 * 12:
        return -1

    cmd = 'ssh ' + usr + ' grep download /home/odatsrv/run_log/upload_ati_csv_from_datastore_tables.log | wc'
    cmd = cmd.split(' ')
    result = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0]
    return int(result.split()[0])
    
def sendSlack(text):
    SLACK_TOKEN = sys.argv[1] # or a TEST token. Get one from https://api.slack.com/docs/oauth-test-tokens
    usr = sys.argv[2] #channel='@jinfeifan' or '#ckan'

    slack_client = SlackClient(SLACK_TOKEN)
    api_call = slack_client.api_call("im.list")

    # You should either know the user_slack_id to send a direct msg to the user
    user_slack_id = "jinfeifan"

    if api_call.get('ok'):
        slack_client.api_call("chat.postMessage", channel='#ckan',
                                   text=text, as_user=True)
        '''
        for im in api_call.get("ims"):
            print(im.get("user"))
            if im.get("user") == user_slack_id:
                im_channel = im.get("id")
                slack_client.api_call("chat.postMessage", channel=im_channel,
                                           text="Hi Buddy", as_user=True)
        '''
def getCSVUpload():
    
    cmd = 'ssh odatsrv@reg1 grep download /home/odatsrv/run_log/upload_ati_csv_from_datastore_tables.log | wc'
    cmd = cmd.split(' ')
    #result = subprocess.run(['ls', '-l'], stdout=subprocess.PIPE)
    #result.stdout.decode('utf-8')
    result = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0]
    return int(result.split()[0])

def getPDUpdate():
    cmd = 'ssh odatsrv@portal2 grep Cleared /home/odatsrv/run_log/rebuild_pd_solr_from_uploaded_csv.log | wc'
    cmd = cmd.split(' ')
    #result = subprocess.run(['ls', '-l'], stdout=subprocess.PIPE)
    #result.stdout.decode('utf-8')
    result = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0]
    return int(result.split()[0])

def main():
    if getCSVUpload() ==15:
        text = ' csv ok :white_check_mark:,'
    else:
        text = ' csv :rage: :rage: *BAD* :rotating_light: :rotating_light:,'

    if getPDUpdate() ==9:
        text += ' solr ok :white_check_mark:.'
    else:
        text += ' solr :rage: :rage: *BAD* :rotating_light: :rotating_light:.'

    count = 5
    while count > 0:
        count -= 1
        try:
            sendSlack2(text)
            #sendSlack(text)
            break
        except:
            pass

if __name__=='__main__':
    main()

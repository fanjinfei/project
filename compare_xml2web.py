#!/usr/bin/env python

import json
import os, sys
from lxml import etree
import csv
import requests
import time
'''
curl -o portal_datasets.out --request POST 'http://solr.host/core_staging_portal/select/' 
  --data 'q=dataset_type%3Adataset&version=2.2&start=0&rows=118000&indent=on&fl=id'
'''
# http://f7oddataregb1.statcan.gc.ca:8080/rc_reg/select/?q=extras_ready_to_publish%3Afalse%20extras_portal_release_date%3A[*+TO+*]&version=2.2&start=0&rows=2000&indent=on&fl=id+extras_ready_to_publish+extras_portal_release_date+extras_org_title_at_publication
def read_solrXml(filename):
    root = etree.parse(filename).getroot()
    i = 0
    solr_refs = []
    data_dict = {}
    for element in root.iter("doc"):
        i += 1
        if i > 2 and False:
            break
        id,org = None,None
        #print (etree.tostring(element))
        for l in element.iter("str"):
            if l.attrib['name']=="id":
                id = l.text
                solr_refs.append(l.text)
            if l.attrib['name']=="extras_org_title_at_publication":
                org = l.text
        data_dict[id] = org
    return solr_refs, data_dict

'''
 psql -U <user> -d <db name> -h db.host -t -A -F","
   -c "select id from package where state='active' and type='dataset'" > ~/tmp/opendata_datasets_4.csv
'''
def read_csv(filename):
    content=[]
    with open(filename) as f:
        reader = csv.reader(f)
        for x in reader:
            if x:
                content.append(x[0].strip())
    return content

def write_csv(filename, ids, data_dict):
    with open(filename, 'wb') as f:
        writer = csv.writer(f)
        for x in ids:
            writer.writerow(['http://registry.open.canada.ca/en/dataset/'+x,
                'http://registry.open.canada.ca/fr/dataset/'+x, data_dict[x]])

def diffIds(solrIds, csvIds):
    return list (set(csvIds) - set(solrIds))

def getRecordDetails(id):
    url='http://registry.open.canada.ca/api/action/package_show?id='
    url='http://open.canada.ca/data/api/action/package_show?id='
    url += id
    retry = 5
    while True:
        try:
            r = requests.get(url, timeout=30)
            break
        except (requests.exceptions.ReadTimeout,
            requests.exceptions.ConnectionError,
            requests.exceptions.Timeout):
            retry -= 1
            if retry <=0:
                print(id, 'timeout')
                return (id, None)
            time.sleep(5)
    try:
        res = r.json()
        res = res['success']
        if not res:
            print('not found', id)
        return (id, res)
    except:
        print(id, 'query error')
        return (id, None)
tids = [
('not found', 'b24e34d0-2e2c-4d97-9022-6b00288213b3'),
('not found', 'fc34fde9-dbea-4a1f-8900-111e5712e35b'),
('not found', 'f6825371-1016-4700-9c10-f5bace3ae70f'),
('not found', 'f2e3f796-9af1-4237-8c5b-10456b12acf3'),
('not found', 'ec43d4e9-22bf-458c-be60-b57848a80af6'),
('not found', 'e4162d17-ef0f-462c-9bc3-74c6437d03c6'),
('not found', 'd7c89667-d796-4251-88c3-49df8a503155'),
('not found', 'ca0928fb-bf95-47c2-90fa-5ca93bcd710c'),
('not found', 'be6e0c89-6d47-4b38-8ea2-120375a1a308'),
('not found', 'b526af57-d57f-4986-a26d-8248a25fee76'),
('not found', 'a03fab34-d78c-4cdc-987a-ba7a1a19036d'),
('not found', '9883bb60-dba9-4e77-b5ef-2a1c626b377d'),
('not found', '95af6326-5826-4310-a202-4c80b0b862d6'),
('not found', '8b89480b-e264-421b-b1fa-591a9e23ffe4'),
('not found', '89396d60-b1d8-4e40-ac5f-ea66f477e584'),
('not found', '81cd996a-9814-4371-9af3-93e04a66d309'),
('not found', '72b3b410-2db0-4cc5-a7ab-2fd64b91b6c1'),
('not found', '6c50f7ad-1140-4df5-b262-fdb2408846a6'),
('not found', '6b1d7bf8-0ce3-4687-b503-e4dc72cff9c6'),
('not found', '64d74786-3f82-4e45-bef8-e1cfdc7cb006'),
('not found', '645c42ba-5dc3-412e-aa3c-e37995354cb8'),
('not found', '5e6999f6-2ae3-4d64-9814-baa21d3eebc3'),
('not found', '5469c026-60d2-43b3-b2eb-41bfab4e9e27'),
('not found', '4f58692a-bbed-4727-95be-fbc8c1508ea4'),
('not found', '3c89441b-9aaa-4cf4-ab8f-6e27b7e95e42'),
('not found', '2de56a8a-1783-4288-9303-8087d933b80d'),
('not found', '1e306232-768e-4288-a15f-84418401a3fa'),
('not found', '1b75dc6c-8850-4d3a-9334-cb27a473f923'),
('not found', '17ff327d-acf3-49a9-95f2-6ab230b6df75'),
('not found', '0bc16fd4-b596-4ab8-a3f3-89da6ccd32c2'),
('not found', 'b51c7687-7608-41cd-a0ea-a88904df1254'),
]
def main():
    print (sys.argv)
    sids, data_dict = read_solrXml(sys.argv[1])
    print(len(sids), sids[:5])

    if False:
        res = []
        for id in sids:
            res.append( getRecordDetails(id) )
        dids = []
        for r in res:
            if r[1]!= True:
                dids.append(r[0])
    dids = [y for x,y in tids]
    write_csv('/tmp/ids.csv', dids, data_dict)
main()

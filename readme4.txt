#sync dataset between portal/registry
1. curl -o 91db3.out http://open.canada.ca/data/api/action/package_show?id=91db3739-3db8-45ca-9a97-3d8a7ce77ae3 #then edit the file only use the results part
  or
ckanapi dump dataset <id1> <id2> -c <conf>
2. ckanapi load datasets -I ~/src/data/91db3.out  -r "http://registry.open.canada.ca"  -a "8xxx"
  or
ckanapi load datasets -I <file> -c <conf>


#solor data
curl -o cra.solr.out --request POST 'http://f7oddatab1.xxxx:8080/contracts/select/' --data 'q=org_name_en%3A"Canada+Revenue+Agency"%0D%0A&version=2.2&start=0&rows=10&indent=on'

#sync organization
 ckanapi dump organizations --all -r http://open.canada.ca/data | bin/transitional_orgs_filter.py | ckanapi load organization -c ../ckan/development.ini

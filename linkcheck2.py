#!/usr/bin/env python3
import asyncio
import requests
from functools import partial
import sys
from binascii import hexlify, unhexlify
# from urllib2 import urlopen # Python 2
from urllib.request import urlopen # Python 3
from requests.models import Response
import urllib
import urllib.parse as urlparse

import os

proxy= os.environ['http_proxy']

def url_fix(s, charset='utf-8'):
    """Sometimes you get an URL by a user that just isn't a real
    URL because it contains unsafe characters like ' ' and so on.  This
    function can fix some of the problems in a similar way browsers
    handle data entered by the user:

    >>> url_fix(u'http://de.wikipedia.org/wiki/Elf (Begriffsklärung)')
    'http://de.wikipedia.org/wiki/Elf%20%28Begriffskl%C3%A4rung%29'

    :param charset: The target charset for the URL if the url was
                    given as unicode string.
    """
    #if isinstance(s, unicode):
    #if isinstance(s, str):
    #    s = s.encode(charset, 'ignore')
    scheme, netloc, path, qs, anchor = urlparse.urlsplit(s)
    path = urllib.parse.quote(path, '/%')
    qs = urllib.parse.quote_plus(qs, ':&=')
    return urlparse.urlunsplit((scheme, netloc, path, qs, anchor))


def test_ftp(url):
    res = Response()
    try:
        req = urllib.request.Request(url)
        if proxy:
            req.set_proxy(proxy, 'http')
        response = urlopen(req, timeout=30)
        chunk = response.read(16)
        print(hexlify(chunk).decode('utf-8'), len(chunk))
        res.status_code = 200
    except:
        print('Error link', url)
        res.status_code = 400
    return res

USER_AGENT="open.canada.ca dataset link checker; abuse report open-ouvert@tbs-sct.gc.ca"
def do_something3(response,  *args, **kwargs):
    print (3, response.url, response.status_code)
    if response.status_code == requests.codes.ok:
        count = 0
        for line in response.iter_content():
            count += (len(line))
            if count > 0:
                #print( count)
                response.close()
                break

url = 'http://data.gc.ca/commonwebsol/fileuploads/2/D/5/2D5184D0-B8F9-43A9-8C41-0C8777337ADF/Dictionnaire de données - Français.txt'
# url= "http://open.canada.ca/commonwebsol/fileuploads/2/D/5/2D5184D0-B8F9-43A9-8C41-0C8777337ADF/Dictionnaire de donnÃ©es - FranÃ§ais.txt"
# url = 'http://open.canada.ca/commonwebsol/fileuploads/2/D/5/2D5184D0-B8F9-43A9-8C41-0C8777337ADF/Dictionnaire%20de%20donn%C3%A9es%20-%20Fran%C3%A7ais.txt'
url0 = 'ftp://ftp.nrcan.gc.ca/ess/sgb_pub/sgb_datasets/sk/THE_KEY_IR_65/THE_KEY_IR_65_DWG.zip'
url0 ='http://gdr.agg.nrcan.gc.ca/gdrdap/dap/index-fra.php?db_project_no=164'
url0= 'file://ftp.nrcan.gc.ca/ftp/ess/sgb_pub/sgb_datasets/ab/PEERLESS_TROUT_INDIAN_RESERVE_NO_238/PEERLESS_TROUT_INDIAN_RESERVE_NO_238_SHP.zip'
url0 = 'http://Canadian National Tornado Database: Verified Events (1980-2009) - Public GIS FR'

@asyncio.coroutine
def main0(results):
    loop = asyncio.get_event_loop()
    future2 = loop.run_in_executor(None, test_ftp, url0)
    future3 = loop.run_in_executor(None, partial(requests.get, hooks = {'response' : do_something3},
                headers={"user-agent":USER_AGENT, 
                        "Accept-Encoding":"gzip, deflate"}, stream=True), url0 )
    future4 = loop.run_in_executor(None, partial(requests.get, hooks = {'response' : do_something3},
                headers={"user-agent":USER_AGENT}, stream=True), url0 )
    response2 = yield from future2
    response3 = response4 = None
    try:
        response4 = yield from future4
    except (requests.exceptions.InvalidURL, requests.exceptions.InvalidSchema):
        print('invalid URL')

    try:
        response3 = yield from future3
    except requests.exceptions.InvalidSchema:
        print('Invalid schema')
    except requests.exceptions.InvalidURL:
        print('invalid URL')
    print(response2, response3, response4)


@asyncio.coroutine
def main(results):
    loop = asyncio.get_event_loop()
    future1 = loop.run_in_executor(None, partial(requests.get, hooks = {'response' : do_something3},
                headers={"user-agent":USER_AGENT}, stream=True), 'http://www.google.com' )
    #future2 = loop.run_in_executor(None, requests.get, 'http://www.google.co.uk')
    future2 = loop.run_in_executor(None, partial(requests.get, hooks = {'response' : do_something3},
                headers={"user-agent":USER_AGENT}, stream=True), url)
    future3 = loop.run_in_executor(None, partial(requests.get, verify=False, timeout=10),'https://127.0.0.1')
    future4 = loop.run_in_executor(None, test_ftp, 'ftp://ftp.nrcan.gc.ca/ess/sgb_pub/sgb_datasets/bc/KASIKA_36/KASIKA_36_SHP.zip')
    response1 = yield from future1
    try:
        response2 = yield from future2
    except:
        response2 = Exception()
    response3 = yield from future3
    response4 = yield from future4
    print(type(response1))
    print(type(response4))
    print(response2)
    results.append(response1)
    results.append(response2)
    results.append(response3)
    results.append(response4)

@asyncio.coroutine
def main2(results):
    loop = asyncio.get_event_loop()
    futures = []
    for i in range (0, 500):
        future = loop.run_in_executor(None, partial(requests.get, hooks = {'response' : do_something3},
                headers={"user-agent":USER_AGENT}, stream=True), 'http://127.0.0.1' )
        futures.append(future)
    for f in futures:
        res = yield from f
        results.append(res)
        
results = []
loop = asyncio.get_event_loop()
#tasks = asyncio.gather(main(results), return_exceptions=False)
#loop.run_until_complete(tasks)
loop.run_until_complete(main0(results))

for i in range(0, 500):
    break
    results = []
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main2(results))
    print('Done loop {0}'.format(i), results[0])

print([ r if not type(r) is Exception else "IsExcept" for r in results])

arm compute library 18.03
https://arm-software.github.io/ComputeLibrary/latest/index.xhtml#S3_2_1_library

build library:
sudo apt install scons

#linux 64bit opencl only
scons Werror=1 -j8 debug=0 asserts=1 neon=0 opencl=1 embed_kernels=1 os=linux arch=arm64-v8a


build example:
arch64-linux-gnu-g++ examples/cl_convolution.cpp utils/Utils.cpp -I. -Iinclude -std=c++11 -L. -larm_compute -larm_compute_core -lOpenCL -o cl_convolution -DARM_COMPUTE_CL

example:
https://bitbucket.org/Anteru/opencltutorial/src/cb1df4439f83779f6132a77e091c9688886f04bf/main.cpp?at=default&fileviewer=file-view-default

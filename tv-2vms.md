#How to use TinyVisor for 2 VMs

This page explains the procedure to create two VMs (VM 0 and VM 1) in the PC and operate the OS with each VM.

Prepare HBA (AHCI card etc.) and HDD, NIC, video card, display etc. to be assigned to VM 1 in advance. Also, refer to the installation method of TinyVisor and install TinyVisor.

##1. Check the hardware configuration

Start GNU / Linux and check the hardware configuration.

First, run the following command and check the number of CPU (logical processor) and APIC ID of each CPU. When assigning a CPU to the VM later, specify the APIC ID.

Grep '^ apicid' / proc / cpuinfo  
Apicid: 0  
Apicid: 2  
Apicid: 4  
Apicid: 6  
Apicid: 1  
Apicid: 3  
Apicid: 5  
Apicid: 7  

Next, execute the following command to check the physical address to which memory is mapped. When allocating memory to the VM later, specify the physical address range.

Grep 'System RAM' / proc / iomem  
00000000-0009c3ff: System RAM  
00100000-dfbdffff: System RAM  
100000000-11fffffff: System RAM  

Finally, execute the following command to check the I / O (PCI) configuration. When allocating I / O to the VM later, specify the bus number, device number, and function number of the Port to which the I / O is connected. For example, to assign "88 SE 9123 PCIe SATA 6.0 Gb / s controller", specify 00: 1 c.2.

Lspci -tv  
- [0000: 00] - + - 00.0 Intel Corporation Core Processor DMI  
           + - 03.0 - [01] - - 00.0 nVidia Corporation G 98 [GeForce 8400 GS]  
           + - 08.0 Intel Corporation Core Processor System Management Registers  
           + - 08.1 Intel Corporation Core Processor Semaphore and Scratchpad Registers  
           + - 08.2 Intel Corporation Core Processor System Control and Status Registers  
           + - 08.3 Intel Corporation Core Processor Miscellaneous Registers  
           + - 10.0 Intel Corporation Core Processor QPI Link  
           + - 10.1 Intel Corporation Core Processor QPI Routing and Protocol Registers  
           + -1 a.0 Intel Corporation 5 Series / 3400 Series Chipset USB Universal Host Controller  
           + -1 a.1 Intel Corporation 5 Series / 3400 Series Chipset USB Universal Host Controller  
           + -1a.2 Intel Corporation 5 Series / 3400 Series Chipset USB Universal Host Controller  
           + -1 a.7 Intel Corporation 5 Series / 3400 Series Chipset USB 2 Enhanced Host Controller  
           + -1 b.0 Intel Corporation 5 Series / 3400 Series Chipset High Definition Audio  
           + -1 c.0 - [02] -  
           + -1 c.1 - [03] - - 00.0 Realtek Semiconductor Co., Ltd. RTL 8111/8168 B PCI Express Gigabit Ethernet controller  
           + -1 c.2 - [04] - - 00.0 Marvell Technology Group Ltd. 88 SE 9123 PCIe SATA 6.0 Gb / s controller  
           + -1 c 3 - [05] - - 00.0 NEC Corporation uPD 720200 USB 3.0 Host Controller  
           + -1 d.0 Intel Corporation 5 Series / 3400 Series Chipset USB Universal Host Controller  
           + -1 d.1 Intel Corporation 5 Series / 3400 Series Chipset USB Universal Host Controller  
           + -1 d.2 Intel Corporation 5 Series / 3400 Series Chipset USB Universal Host Controller  
           + -1 d.3 Intel Corporation 5 Series / 3400 Series Chipset USB Universal Host Controller  
           + -1 d.7 Intel Corporation 5 Series / 3400 Series Chipset USB 2 Enhanced Host Controller  
           + -1 e.0 - [06] - - 03.0 ATI Technologies Inc Rage XL  
           + -1 f.0 Intel Corporation 5 Series Chipset LPC Interface Controller  
           + -1 f.2 Intel Corporation 5 Series / 3400 Series Chipset 6 port SATA AHCI Controller  
           \ -1 f.3 Intel Corporation 5 Series / 3400 Series Chipset SMBus Controller  

##2. Install OS on HDD for VM1

First, in the BIOS menu, set the boot order of the HDD assigned to VM 1 above the boot order of VM 0 HDD.

Next, install the OS to that HDD in the usual procedure. At this time, please install OS for BIOS environment, not for UEFI environment.

Finally, in the BIOS menu, restore the boot order.  

##3. Assign CPU, memory, I / O to each VM

In order to generate VM 1 and assign CPU, memory, I / O to VM 1, specify the following VMM parameters.  

| Parameter  | Description   | Example  |  
| --- | --- | --- |  
| `Vm = <vm name> [, <vm name> [, ...]]` | Specify the list of VMs to generate. Vm 0 is assumed to be specified without specifying it.  |  Vm = vm 0, vm 1  |  
| `<Vm name>. Cpu = <APIC_ID> [, <APIC_ID>, [...]]` | Specifies the APIC ID of the CPU to be assigned to the specified VM.    |      Vm1.cpu = 6.7  |
| `<Vm name> .mem = <start address> - <end address> [, [...]]` | Specifies the address range of the physical memory to be assigned to the specified VM.   |    Vm1.mem = 80000000-11fffffff  |
| `<Vm name> .pci =  <BUS>: <DEV>: <FUNC>, [...]]`  | The bus number of the PCIe Root Port to be assigned to the specified VM, <vm name>. Cpu = <BUS> Specify the device number and function number. |  Vm 1. Pci = 00: 1 c. 1, 00: 1 c. 2, 00: 1. 3, 00: 1. 4  |

For example, the VMM parameters are as follows.

Vm = vm0, vm1 vm1.mem = 80000000-11fffffff vm1.cpu = 6, 7 vm1.pci = 00: 1c.1, 00: 1c.2, 00: 1c.3, 00: 1c.4


The specific procedure depends on the method of installing VMM.

In the UEFI environment, if you installed TinyVisor in the USB memory, go to the directory where you built TinyVisor and execute the following command to write it to the USB memory again. For 1.7 or later, you need to explicitly specify the guest BIOS binary.

After 1.7

Sudo vmm / boot / uefi - loader / install.sh - c 'vm = vm 0, vm 1 vm 1 .em = 80000000 - 11 efffffff vm 1. Cpu = 6, 7 vm 1. Pci = 00: 1 c. 1, 00: 1 c. 2, 00: 1 c 3, 00: 1 c 4 '- m bios / out / bios.bin / dev / sdf 1

Before 1.6

Sudo ./install_to_usb.sh -e -c 'vm = vm0, vm1 vm1.mem = 80000000-11fffffff vm1.cpu = 6, 7 vm1.pci = 00: 1c.1, 00: 1c.2, 00: 1c. 3,00: 1c. 4 '/ dev / sdf 1


In the BIOS environment, if you install TinyVisor on the USB memory, change to the directory where TinyVisor was built and execute the following command to write it to the USB memory again. In the BIOS environment, specify "vm0.boot_int18" or "vm0.boot_drive = 81" and load the OS from the next priority drive of the USB memory.

After 1.7

Sudo vmm / boot / loader / install.sh - c 'vm 0. Boot_int 18 vm = vm 0, vm 1 vm 1 .em = 80000000 - 11fffffff vm 1. Cpu = 6, 7 vm 1. Pci = 00: 1 c. 1, 00: 1 c. 2 , 00: 1c. 3,00: 1. 4 '/ dev / sdf 0 1 vmm / boot / loader / bootloader vmm / vmm.elf bios / out / bios.bin

Before 1.6

Sudo ./install_to_usb.sh -c 'vm0.boot_int18 vm = vm0, vm1 vm1.mem = 80000000-11fffffff vm1.cpu = 6, 7 vm1.pci = 00: 1c.1,00: 1c.2,00: 1c . 3, 00: 1 c 4 '/ dev / sdf


When installing TinyVisor on the Linux file system, change the entry of / etc / grub.d / 40_custom as follows, then use the update-grub command or the grub2-mkconfig -o / boot / grub2 / grub.cfg command Execute.

Menuentry 'VMM (vm 0, vm 1)' - class v mm {  
        Insmod part_msdos  
        Insmod ext 2  
        Set root = '(hd 0, msdos 1)'  
        Echo 'Loading VMM ...'  
        Multiboot / boot / vmm.elf vm = vm 0, vm 1 vm 1.mem = 80000000 - 11 ffffffff vm 1. Cpu = 6, 7 vm 1. Pci = 00: 1 c 1, 00: 1 c 2, 00: 1 c 3, 1c.4  
        Echo 'Loading guest BIOS ...'  
        Module / boot / bios.bin  
}  

##4. Start up VMM, OS

Turn on power to the PC.

When TinyVisor is installed in USB memory, VMM will start up automatically. When VMM is loaded from the USB memory and start up, the message is output to COM1 or screen (default is COMT1).

When installing TinyVisor on the Linux file system, select "VMM (vm 0, vm 1)" on the GRUB screen and activate VMM.

The following is a picture when TinyVisor is installed in USB memory, HDD assigned FreeBSD is assigned to VM 0, HDD assigned Linux is allocated to VM 1.

When VMM startup is completed, execution of VM0 and VM1 starts.

In VM 0, the boot loader is loaded by the Host BIOS, the OS is loaded by the boot loader, and the OS starts up. The boot loader menu and OS message are output to the screen.

In VM 1, the Guest BIOS message is output to COM 1 at the beginning. Next, GRUB is loaded by Guest BIOS, OS is loaded by GRUB, OS starts up. If you assign a video card to VM1, OS messages are output on the screen of the display connected to the video card.

Once the OS of VM 0 and VM 1 has been activated, it can operate independently.

##5. Restart the OS

In the BIOS environment, when restarting the OS of VM 0, only the OS of VM 0 restarts. Restarting the OS of VM 1 restarts only the OS of VM 1.

In the UEFI environment, when restarting the OS of VM 0 and shutting down the OS of VM 1, the entire machine restarts. When restarting the OS of VM 1, only the OS of VM 1 restarts.

##6. Stopping the OS

If you stop both the VM 0 OS and the VM 1 OS, the PC turns off.

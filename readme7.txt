#use and custome tinyCore linux 8.0 inside Virutalbox 5.0.8

1. Download ISO (TinyCorePure64-8.0.iso, not mini/plus)
2. Install as VM with sda/sdb 8G, 2G memory
   2.1 frugal install to sda, has a /mnt/sda1/tce folder for downloaded tce
	boot CD
	export http+proxy
	tce-load -wi tc-install-GUI (tc-install)
	tc-install
   2.2 backup session with backup/restore tool, change term font size first(large)
        Aterm*font: -misc-fixed-medium-r-normal-*-24--*-*-c-*-iso8859-1
	(xlsfonts to list all fonts on your system)
   2.3 bootcode
       grub.cfg OR //mnt/sda1/tce/boot/extlinux/extlink.conf for isolinux
       laptop text(to console)
3. Prepare for bulding kernel module
  aps install tc-install compiletc xorg-7.7 kernel-env
  3.1 download tc 8.0/x86_64/src/kernel-4.8.17-patched.txz, config-4.8.17-rinycore64
     tar Jxvf kernel
     copt config-.. linux-4.8.17
     cd linux-4.8.17
     make oldconfig
     make modules
     make modules_install
     sudo mkdir /usr/src
     sudo ln -sf /mnt/sdb1/linux-4.8.17 /usr/src/linux

    ### clean install original-modules-4.8.17-tinycore64.tcz
  3.2 download vbox guest addon 5.1.21 (nightbuild, this works with latest linux kernel)
    sudo ln -sf /lib /lib64
    sudo ./VboxLinuxAddon.run --target vboxsrc
  3.3 test #xserver 1.19 uses KMS driver, no usespace video driver needed. !!BUT!!, llibgx report error 3D no
        vboxvideo_drv.so found
    $ killall Xorg
    $ sudo modprobe vboxvideo
    $ sudo VBoxClient --display #can resize now ( first line in ~/.xsession)
        ( VBoxClient --clipboard)
    $ startx
  3.4 use bigger font for Xorg
  3.5 use bigger font for firefox

4. make persistent
  4.1 find files installed (No need to do on a clean sys, re-install is ok)
        $ touch mymarker
        $ sudo ./VBoxLinuxAdditions.run
        $ sudo find / -not -type 'd' -cnewer mymarker | grep -v "\/proc\/" | grep -v "^\/sys\/" | tee files
        $ beaver [or some other text editor] files
    decide which file can be deleted
  4.2 make a tar from the above list
        $ sudo tar -T files --numeric-owner -zcvf vb_guest_additions.tar.gz
        $ mkdir /tmp/pkg
        $ cp vb_guest_additions.tar.gz /tmp/pkg
        $ cd /tmp/pkg
        $ sudo tar xf vb_guest_additions.tar.gz
        $ rm vb_guest_additions.tar.gz
        $ cd ..
        $ sudo mksquashfs pkg/ vb_guest_additions.tcz
        $ sudo chown tc:staff vb_guest_additions.tcz
        $ cp vb_guest_additions.tcz /mnt/sda1/tce/optional
  4.3 boot change
     add vb...tcz to /mnt/sda1/onboot.lst
     add `sudo ln -sf /lib /lib64` to ~/.profile
     ?? sudo VBoxClient --display ?? where to put this? before startx(~/.xsession)?

5. Other commands
  5.1 firefox need gtk2
	export GTK_IM_MODULE=xim  #even in SCIM
  5.2 Backup/Restore
    filetool .filetool.lst .xfiletool.lst (@ /opt)
    export BACKUP=0 (@~/.profile)
    
  5.3 languge
    tce-load -iw getlocale (all zh_CN)
    getlocale.sh
    add boot option 'lang=zh_CN.UTF-8' to file /mnt/sda1/tce/boot/extlinux/extlinux.conf
    #input methods: SCIM
    #   scim scim-pinyin (from github), add following to ~/.profile
    export XMODIFIERS=@im=scim
    export GTK_IM_MODULE=scim
    export QT_IM_MODULE=scim
    scim -d # maybe after ~/.xsession for tc

  5.4 ibus, python
     gi module == pygobject pygobject-interop, NOT gist

  5.5 cmake extra (ECM)
	git clone git://anongit.kde.org/extra-cmake-modules
	cd extra-cmake-modules
	mkdir build
	cd build
	cmake .. # or run : cmake -DCMAKE_INSTALL_PREFIX=/usr .. &&
	make
	sudo make install
   
  gtk-query-immodules-2.0 | sudo tee /usr/local/etc/gtk-2.0/gtk.immodules
  gtk-query-immodules-3.0 --update-cache (after install scim)

  5.6 gtk-2, gtk-3(for firefox >=53) im
    http://ftp.acc.umu.se/pub/gnome/sources/gtk+/3.18/
    recompile gtk+-2.24.31 gtk+-3.18.9:
	dependency: PyGObject, GObject, gobject-intro...(???)
	for gtk3, `./configure --enable-include-immodules=xim` to have some imm

  5.7 ubuntu build vanilla kernel
    sudo apt-get install kernel-package git libssl-dev
    make menuconfig
    fakeroot make-kpkg --initrd --append-to-version=-patched kernel-image kernel-headers -j $(getconf _NPROCESSORS_ONLN)
    #goback to parent director and install `dpkg -i`

  5.8 sh/bash (tar, sed, readlink), laptop power (/sys/class/../BAT0/energy_noe/full)
    install tce (tar, sed, binutils), rm link from /bin/(sh, tar, sed, readlink) to busybox
    exec echo freeze | sudo tee /sys/power/state
    'xx' not found: chmod 777; chown tc:staff; ldd (see first lib exist); /lib64
    save .wbar to  /home/tc/yy; vi .xsession, killall wbar, replace new .wbar, then wbar&
    default tce is /etc/sysconfig/tcedir
    qt5(qtdiag need libwacom on TC)

  5.9 chrome linux
    download https://commondatastorage.googleapis.com/chromium-browser-snapshots/Linux_x64/467335/chrome-linux.zip
    need GConf-2.32.4 ORBit2-2.14.19, (intltool, dbus-glib-dev,..),
    patch ORbit2 (remove "-DG_DISABLE_DEPRECATED" from CPPFLAGS in linc2/src/Makefile.in

6. grub2-install EFI and BIOS multi
  6.1. convert MBR to GPT (mount new disk as sdd, make sure only one partition(or not edit to bad alignment)
    #sudo gdisk /dev/sdd
  6.2. use `sudo gparted /dev/sdd`
    resize sdd1, followed with 512M free space
    create sdd2 (esp,boot) flag, fat32
    create sdd3 (bios_grub) flag unformat
  6.3 prepare boot file
    #sudo mkdir /mnt/sdd1/boot
    #sudo cp coreppure64.gz /mnt/sdd1/boot
    #sudo cp vmlinuz64 /mnt/sdd1/boot

  6.4. install efi boot, first mount to /mnt/sdd1 /mnt/sdd2
    sudo grub-install --target=x86_64-efi --removable --boot-directory=/mnt/sdd2/EFI/BOOT --efi-directory=/mnt/sdd2
    sudo grub-install --target=i386-pc  --boot-directory=/mnt/sdd1/boot /dev/sdd

  6.5 create 2 grub.cfg files for efi and bios, run grub-script-check to verify syntax
    For EFI: (uuid is the /dev/sdd1, read from gparted /dev/sdd1)
FILE /EFI/BOOT/grub.cfg (ubuntu grub2 created)
change root -> efiroot
FILE: /EFI/BOOT/grub/grub.cfg
loadfont unicode
insmod efi_gop
insmod part_gpt
insmod ext2
set gfxterm=auto
terminal_output gfxterm

search --no-floppy --fs-uuid --set=root --hint-efi=hd0,gpt1 f8422ac1-fef4-48ce-b79e-abce5a598e8a

menuentry "corepure64" {
  linux /boot/vmlinuz64 quiet noswap tce=UUID=f8422ac1-fef4-48ce-b79e-abce5a598e8a waitusb=10:UUID=f8422ac1-fef4-48ce-b79e-abce5a598e8a tz=GMT-4 blacklist=bcma blacklist=ssb blacklist=b43
  initrd /boot/rootfs64.gz
  #initrd /boot/rootfs64.gz /boot/modules64.gz
}

menuentry "tinyvisor" {
  insmod chain
  search --no-floppy --fs--uuid --set=root ABCD-EFGH #efi gpt UUID
  chainloader/EFI/Boot/loadvmm.bin
}

  For Bios:
loadfont unicode
insmod part_msdos
insmod ext2
set gfxterm=auto
terminal_output gfxterm

search --no-floppy --fs-uuid --set=root f8422ac1-fef4-48ce-b79e-abce5a598e8a

menuentry "corepure64" {
  set root=(hd0,1) 
  # (hd0,2) if bios boot is on /dev/sdd2
  linux /boot/vmlinuz64 quiet noswap tce=UUID=f8422ac1-fef4-48ce-b79e-abce5a598e8a waitusb=10:UUID=f8422ac1-fef4-48ce-b79e-abce5a598e8a tz=GMT-4 blacklist=bcma blacklist=ssb blacklist=b43
  initrd /boot/rootfs64.gz
  #initrd /boot/rootfs64.gz /boot/modules64.gz
}

menuentry "TinyCore 6.1 Pure 64" {
 set isofile=/iso-images/TinyCorePure64-6.1.iso
 loopback loop $isofile
 echo Loading vmlinuz64 ...
 linux (loop)/boot/vmlinuz64 -- tz=CET-1CEST,M3.5.0/2,M10.5.0/3 iso=LABEL="Intenso"$isofile cde nozswap swapfile loglevel=3 vga=791
 echo Loading corepure64.gz ...
 initrd (loop)/boot/corepure64.gz
}

7. build rootfs (busybox + [/dev/ /proc/ /sys /tmp ...] + core-scripts?)
  1. see existing rootfs
    #gunzip -c rootfs.gz > /tmp/my_rd
    #cpio -it < /tmp/my_rd  [ list only ]
    # cd [somewhere]
    # cpio -i < /tmp/my_rd
 2 create min initramfs (need to config kernel to support initrd)
   [ create /init ]
'''
#!/bin/sh
/bin/mount -t proc none /proc
/bin/mount -t sysfs sysfs /sys
cat <<'EOF'
Welcome to mini Linux
  _ _                  
 | (_)_ __  _   ___  __
 | | | '_ \| | | \ \/ /
 | | | | | | |_| |>  < 
 |_|_|_| |_|\__,_/_/\_\
                       
EOF
echo 'Enjoy this little puppy!'
echo '~~~~~~~~~~~~~~~~~~~~~~~~'
echo ''
/bin/sh
'''
   2.1 /dev
	 cp -dpR /dev/tty[0-6] /mnt/dev [and other audio/sda/sdb/]
	OR mknod
   2.2 /etc
	fstab [include tmpfs sys proc]
   # find . -print0 | cpio --null -ov --format=newc > ../initramfs.cpio [or specify the director in kernel config]
   # find lib usr | cpio -o -H newc | gzip -9 > ../modules${is64}.gz  [example]

8. Build hybrid dual boot ISO image for release
  reference link: https://fedoraproject.org/wiki/User:Pjones/BootableCDsForBIOSAndUEFI
  1. # copy existing files
     mkdir iso
     mount <release tc.iso> <to local dir>
     cp <iso content> iso/
  2. make iso
      mkisofs -U -A "MyTC8 x86_64" -V "MyTC8 x86_64 Disc" \
         -volset "MyTC8 x86_64" -J -joliet-long -r -v -T -x ./lost+found \
         -o ../mytc8-dualboot.iso \
         -b boot/isolinux/isolinux.bin -c boot/isolinux/boot.cat -no-emul-boot -boot-load-size 4 \
         -boot-info-table -eltorito-alt-boot -e EFI/BOOT/efiboot.img -no-emul-boot .

grub/Linux boot debug tricks:
under GRUB2 menu, enter command:
```
set pager=1  
set debug=all  
set default=0/1/2
set timeout=0/10/-1
```
Linux: nosplash debug (remove quiet)

#Linux grub boot without VGA card, use this config (fake serial, not exist is ok)
```
default=0
timeout=5
#splashimage=(hd0,0)/grub/splash.xpm.gz
serial --unit=0 --speed=9600
terminal_input serial, console; terminal_output serial, console
sleep --interruptible --verbose 60

trick:
chainloader ...
boot
sleep 10
chainloader ...
```

#linux boot code for serial console 
	linux console=tty1 console=ttyS0,9600 #this is under grub menu
#linux serial console: /etc/inittab (need to remix rootfs)
#For tinycore, do no add `-nl autologin`, it will fail


2 intel GVT repo
Old Direct display: https://github.com/01org/igvtg-kernel
Main Need RDP: https://github.com/01org/gvt-linux.git

monitor GPU resource

# sudo lspci -vnn | grep VGA -A 12
# sudo lshw -numeric -c display

sudo apt install intel-gpu-tools
# intel_gpu_top
# intel_gpu_time

#nmcli dev show  -- get dns/gateway from cli on Ubuntu

#Change VM default NAT DHCP IP range
VBoxManage modifyvm  "NameOfVM" --natnet1 "192.168/16" 

import requests
from bs4 import BeautifulSoup as mparser
import sys
import json
import time

class Fess():
    def __init__(self, host, token):
        self.host = host.rstrip('/') +'/api/admin'
        self.s = requests.Session()
        self.headers = {'Content-Type':'application/json',
                        'Authorization':token}
        self.crawling_logs = {}
    def scheduler(self, name):
        url = self.host +"/api/admin/scheduler/{id}".format(id=name)
        url = self.host +"/scheduler/settings"
        r = self.s.get(url, headers=self.headers)
        r= json.loads(r.text)['response']['settings']
        data = {}
        for rec in r:
            data[rec['name'].strip()] = rec
        self.data = data
        return data
    def scheduler_start(self, name):
        name=self.data[name]['id']
        url = self.host +"/scheduler/{id}/start".format(id=name)
        r = self.s.post(url, headers=self.headers)
        return json.loads(r.text)['response']

    def scheduler_status(self, name):
        name=self.data[name]['id']
        url = self.host +"/scheduler/setting/{id}".format(id=name)
        r = self.s.get(url, headers=self.headers)
        return json.loads(r.text)['response']

    def crawling_logs_name2id(self, name):
        if len(self.crawling_logs) ==0:
            ds = self.get_crawling_logs('*')
        
    def get_crawling_logs(self,name):
        if name == '*': #all the list
            url = self.host +'/crawlinginfo/logs'
            r = self.s.get(url, headers=self.headers)
	    return json.loads(r.text)['response']['logs']
        else:
            url = self.host +'/crawlinginfo/log/' + self.crawling_logs_name2id(name)

fs = Fess(sys.argv[1], sys.argv[2])

#r = fs.get_crawling_logs('*')
r = fs.scheduler('*')
for k,v in r.items():
    print k, v['id']

r = fs.scheduler_status('Data Crawler - daily_latest')
print 'running:', r['setting']['running']

if not r['setting']['running']:
    r = fs.scheduler_start('Data Crawler - daily_latest')
    print 'start:', r

time.sleep(5)
r = fs.scheduler_status('Data Crawler - daily_latest')
print 'running:', r['setting']['running']


'''
# Fill in your details here to be posted to the login form.
payload = {
    'username': 'admin',
    'password': 'admin',
    'login': 'Login',
    'lastaflute.action.TRANSACTION_TOKEN': ' ',
}

headers = {'Content-Type': 'application/json',
        'Authorization':'RiKI10a6b49sYzuDhrONWSh73ktTS3OYaUtzvuixEy4BYxuHXFJ9ixYk3e0I'}

url = 'http://localhost:8080/login/'
url_1 = 'http://localhost:8080/api/admin/webconfig/settings'
#url_1 = 'http://localhost:8080/api/admin/webconfig/details/4'
url_1_1 = 'http://localhost:8080/api/admin/crawlinginfo/logs' #/log/{id}

url_2 = 'http://localhost:8080/json/?q=apache&num=20&sort='

# Use 'with' to ensure the session context is closed after use.
with requests.Session() as s:
    r = s.get(url)
    token = mparser(r.text, "lxml")
    payload['lastaflute.action.TRANSACTION_TOKEN'] =  token.find(name="input", attrs={'name':'lastaflute.action.TRANSACTION_TOKEN'}).attrs['value']

    p = s.post(url, data=payload)
    # print the html returned or something more intelligent to see if it's a successful login page.
    #print p.text

    # An authorised request.
    r = s.get(url_1, headers=headers)
    print json.loads(r.text)['response']['settings'][0]['name']
    print '*** ***'
    r = s.get(url_1_1, headers=headers)
    print r.text

    r = s.get(url_2)
    print r.text
    sys.exit(0)
'''


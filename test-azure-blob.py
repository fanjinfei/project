#!/usr/bin/env python
import sys, os
import mimetypes
from pprint import pprint

user,apikey,container_name = sys.argv[1], sys.argv[2], sys.argv[3]
proxy= os.environ['http_proxy']

from azure.storage.blob import BlockBlobService
from azure.common import AzureMissingResourceHttpError
block_blob_service = BlockBlobService(account_name=user, account_key=apikey)
#block_blob_service.create_container('openinformation')
#block_blob_service.delete_container('devcontainer')

from azure.storage.blob import PublicAccess
#block_blob_service.create_container('devcontainer', public_access=PublicAccess.Container)
#block_blob_service.create_container('devcontainer')

#container = block_blob_service.get_container(container_name)

containers = block_blob_service.list_containers() 
for c in containers:
    print c.name

generator = block_blob_service.list_blobs(container_name)
for blob in generator:
    print(blob.name)
    #_del_blob(blob.name)
    #block_blob_service.get_blob_to_path(container_name, blob.name, '/tmp/a.out')
    #break

print('list done')
os._exit(0)

from azure.storage.blob import ContentSettings

print ('uploading...')
def _m_upload_file(filename, blob_name):
    print ('uploading...', filename, blob_name)
    content_type, _ = mimetypes.guess_type(filename)
    content_settings = None
    if content_type:
        content_settings = ContentSettings(content_type=content_type)
    with open(filename, 'rb') as f:
        block_blob_service.create_blob_from_stream(
            container_name,
            blob_name,
            stream=f,
            content_settings=content_settings
            )
def get_size(blob_name):
    try:
        obj = block_blob_service.get_blob_properties(container_name,
            blob_name
            )
        print (blob_name, 'size', obj.properties.content_length)
    except AzureMissingResourceHttpError:
        print(blob_name, 'not exists')        
def _get_content_type(blob_name):
    try:
        obj = block_blob_service.get_blob_properties(container_name,
            blob_name
            )
        s = obj.properties.content_settings
        print (blob_name, 'content Settings', s.content_type, s.content_disposition)
    except AzureMissingResourceHttpError:
        print(blob_name, 'not exists')        
def _del_blob(blob_name):
    print ('deleting', blob_name)
    block_blob_service.delete_blob( container_name, blob_name)

for blob in generator:
    print(blob.name)
    #_del_blob(blob.name)
os._exit(0)

docs = [
        'High Level Project Phase Summary.docx',
        'Open by Default Pilot - metadata_v3.1.xlsx',
        'Open By Default - GCDOCS To CKAN Protocol - Draft 1.docx',
        'Open By Default - Technical Stream Project Schedule.docx'
        ]
'''names = [
    'resource/12b79506-4d48-484a-9ed2-6e6ea13a08af/download/HighLevelProjectPhaseSummary.docx',
    'resource/0b7d08d3-a6b2-4800-bd15-4b1cac4e4233/download/OpenbyDefaultPilotmetadata_v3.1.xlsx',
    'resource/ff58ac0f-f95f-4fb0-8674-f19fa21847ad/download/OpenByDefaultGCDOCSToCKANProtocolDraft1.docx',
    'resource/758bff28-84fd-4ea7-9b70-5323aec7f61f/download/OpenByDefaultTechnicalStreamProjectSchedule.docx'
]'''
names = [ #pattern: resources/$(UUID)/filename
    'resources/12b79506-4d48-484a-9ed2-6e6ea13a08af/HighLevelProjectPhaseSummary.docx',
]

docs = [
        'dq170529b-eng.pdf', 'dq170529a-eng.pdf', 'dq170529c-eng.pdf',
        'dq170529d-eng.pdf', 'dq170529e-eng.pdf' ]
names = [
    'resources/12b79506-4d48-484a-9ed2-6e6ea13a08af/dq170529beng.pdf',
    'resources/0b7d08d3-a6b2-4800-bd15-4b1cac4e4233/dq170529aeng.pdf',
    'resources/ff58ac0f-f95f-4fb0-8674-f19fa21847ad/dq170529ceng.pdf',
    'resources/758bff28-84fd-4ea7-9b70-5323aec7f61f/dq170529d-eng.pdf'
]

#_del_blob(names[3])
_get_content_type(names[3])
os._exit(0)

#https://openinformation.blob.core.windows.net/openinformation/resources/$(RID)/filename
for i in range(0,4):
    _m_upload_file('/home/jffan/src/data/obd/'+docs[i], names[i])
    get_size(names[i])
    #_del_blob(names[i])
os._exit(0)
    
if True:
    fn = '/tmp/tobedeletedtest.csv'
    f = open('/tmp/tobedeletedtest.csv', 'rb')
    block_blob_service.create_blob_from_stream(
        container_name,
        'resources/788fa176-3fbc-11e7-a8a1-0344bb6b0ee1/tobedeletedtest.csv',
        stream=f,
        content_settings=ContentSettings(content_type='text/csv' if fn[-3:]=='csv' else
                        'application/octet-stream'
                        )
        )
else:
    block_blob_service.create_blob_from_path(
        container_name,
        'resources/788fa176-3fbc-11e7-a8a1-0344bb6b0ee1/tobedeletedtest.csv',
        '/tmp/tobedeletedtest.csv',
        content_settings=ContentSettings(content_type='text/csv')
        )
print('uploaded')
os._exit(0)

block_blob_service.create_blob_from_path(
    container_name,
    'resources/1f1b0e53-40cc-4900-a08a-e5595697aaea/brokenlink.csv',
    '/tmp/broken_link.csv',
    content_settings=ContentSettings(content_type='text/csv')
    )
block_blob_service.get_blob_to_path(container_name,
    'resources/1f1b0e53-40cc-4900-a08a-e5595697aaea/brokenlink.csv',
    '/tmp/broken_link_down.csv',
    )

'''
block_blob_service.create_blob_from_path(
    'mycontainer',
    'myblockblob',
    'sunset.png',
    content_settings=ContentSettings(content_type='image/png')
            )

block_blob_service.get_blob_to_path('mycontainer', 'myblockblob', 'out-sunset.png')

block_blob_service.delete_blob(
    container_name,
    'resources/1f1b0e53-40cc-4900-a08a-e5595697aaea/broken_link.csv'
    )
'''

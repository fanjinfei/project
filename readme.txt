SECTION 1: learning git: https://git-scm.com/docs/gittutorial
git init
git add .
git commit
git add file1 file2 file3
git diff --cached
git status

view history:
git log
git log -p
git log --stat --summary

managing branches:
git branch experimental  (create a branch, git branch -d xxx [delete branch])
git branch (view branch)
(edit a file)
git commit -a

git checkout master (-- checkout <file>: revert local changes in file)
(edit a file)
git commit -a  (now two branches diverged)

git merge experimental (merge changes in experi.. into master)

git diff (will show conflicts, resolve it. then ..)
git commit -a

this is a master text
this is a branch text: resolved

gitk (GUI git)
git branch --delete xxx (this ensures changes in the xxx in already in the current branch)
git branch --delete --force xxx (delete whatever)

--collabrate
git remote add origin git@github.com:username/reponame.git (if it is not clone against remote repository)
git push origin master (-- after first push, simply 'git push')

Collabrate Case study:
bob clone from alice repo to bot.git; bob commit change to bob.git; bob forked as bob.git
alice: cd project; git pull bot.git master -- merge bob's change into alice's current branch
	need to fix conflicts if exist; 'pull' = fetch changes from remote branch + merge into working branch
	normally alice commit changes first
OR alice peek: git fetch bob.git master; git log -p HEAD..FETCH_HEAD
  visualized bob commit history since forked:  gitk HEAD..FETCH_HEAD
  visualized both changes: gitk head...FETCH_HEAD
alice: git add remote bob bot.git (not local repo)
	git fetch bob
	git log -p master..bob/master
	git merge bob/master OR git pull . remotes/bob/master
later bob update his repo with alice's changes:
	git pull

SECTION 2: --sync you forked repo with upstream
git remote add upstream https://github.com/whoever/whatever.git (# Add the remote, call it "upstream")
git remote -v (list remote)

# Fetch all the branches of that remote into remote-tracking branches,
# such as upstream/master:
git fetch upstream

git checkout master (# Make sure that you're on your master branch)

# Rewrite your master branch so that any commits of yours that
# aren't already in upstream/master are replayed on top of that
# other branch:
git rebase upstream/master (do not use 'git merge upstream/master', 
	rebase is more clean, but will rewrite history of your master branch)

git push -f origin master (only need '-f' first time after rebased)


SECTION 3: upload local repo to github
from github GUI create a new repo; from local repo 
$ git remote add origin remote repository URL
$ git push -u origin master (-u is used to track information during push)
	$ git checkout experimental
	$ git push -u origin experimental (push a new branch into remote repo)
git fetch origin master (does not affect local working directory, it does not merge changes like ;git pull')
git diff origin/master..master (see diff between remote/branch vs local branch)

SECTION 4: undo a recent commit
git reset xxxxxx
git checkout .
git push -f .....

SECTION 5: git push exisitng repo to new and different remote
    Create a new repo at github.
    Clone the repo from fedorahosted to your local machine.
    git remote rename origin upstream
    git remote add origin URL_TO_GITHUB_REPO
    git push origin master



undo a recent cached add (directory or file):
git rm -r --cached <adir\>

#pull in latest into working dir
git stash
git pull
git stash apply

#additional stash
git stash list
git stash drop stash@{0}
git stash save "<you message>"

#git ammend specific commit message (sha1)
git rebase -i sha1~1 #use reword

#git diff show tabs
git diff | cat -A

#git rebase failed due to 'It looks like git-am is in progress. Cannot rebase':
rm -rf .git/rebase-apply

#git move one file to another repo, keep the commit history
cd repository
git log --pretty=email --patch-with-stat --reverse --full-index --binary -- path/to/file_or_folder > patch
cd ../another_repository
git am < ../repository/patch 

#git for ms office doc, first install `pandoc`
# .gitattributes file in root folder of your git project
*.docx diff=pandoc

# .gitconfig file in your home folder
[diff "pandoc"]
  textconv=pandoc --to=markdown
  prompt = false
[alias]
  wdiff = diff --word-diff=color --unified=1

then `git wdiff important_file.docx` to see changes. 
`git log -p --word-diff=color important_file.docx` to see all the changes overtime.

#git -c http.sslVerify=false (accept self-signed certificates)

#git config --global core.autocrlf=true
#git config --global core.autocrlf true
#git pull ssh:U@host/path
#unset SSH_ASKPASS

#show remote branch: git branch -a/-r
#pull/push all branches: '--all'

#submodule
1. add submodule
 #git submodule add git@mygithost:billboard lib/billboard
 # git status
2. #git submodule init
3. #git submodule update
4. git submodule rm lib/billboard

#remove all the ^M in file (use Crtl+V Ctrl+M)
#sed -e 's/^M//g' file1 > file2

#!/usr/bin/env python
import sys, os
from pprint import pprint
import libcloud

from libcloud.storage.types import Provider
from libcloud.storage.providers import get_driver

import libcloud.security
libcloud.security.VERIFY_SSL_CERT = False

#import urllib3
#urllib3.disable_warnings()
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

user,apikey,container_name = sys.argv[1], sys.argv[2], sys.argv[3]
proxy= os.environ['http_proxy']

''' Compute Node
cls= libcloud.get_driver(libcloud.DriverType.COMPUTE, libcloud.DriverType.COMPUTE.AZURE)
driver= cls(user, apikey, http_proxy=proxy)
pprint(driver.list_sizes())
pprint(driver.list_nodes())
'''
#cls= libcloud.get_driver(libcloud.DriverType.COMPUTE, libcloud.DriverType.COMPUTE.RACKSPACE)

# Storage
cls = get_driver(Provider.AZURE_BLOBS)


driver= cls(user, apikey, secure=False, http_proxy=proxy) #libcloud.storage.drivers.azure_blobs.AzureBlobsStorageDriver
print(driver)

container = driver.get_container(container_name)
print(container)

#list container obj
i = 0
total_size = 0
#driver.connection.set_http_proxy(proxy_url=proxy) !!! BAD Implementation !!!
for obj in driver.iterate_container_objects(container):
    if i<5: print(obj.name, obj.size)
    break

    # overwrite_existing=False
    driver.download_object(obj, '/tmp/a.out', overwrite_existing=True) # NOT working if secure=True in cls()
    break

    dld_stream = driver.download_object_as_stream(obj, chunk_size=64*1024)
    print(dld_stream)
    for v in dld_stream:
        print('downloaded:', len(v))
    break

    #print(driver.get_object_cdn_url(obj)) NOT implemented
    # http[s]://<container_name>.blob.core.windows.net/<container_name>/<obj.name>

    i += 1
    if i > 5: break
    total_size += int(obj.size)

print( 'total_size:', total_size)
#print(len(driver.list_container_objects(container)))

print('uploading..')
driver.upload_object(
    '/tmp/tobedeletedtest.csv',
    container, 'resources/788fa176-3fbc-11e7-a8a1-0344bb6b0ee1/tobedeletedtest.csv',
    )
print('uploaded')
obj = driver.get_object(container_name, 'resources/788fa176-3fbc-11e7-a8a1-0344bb6b0ee1/tobedeletedtest.csv')
print (obj.size)

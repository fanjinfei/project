# Python tips

#quick debug
import pdb; pdb.set_trace()

#remove orphaned *.pyc
# when switch between branches, previous *.pyc might exists and cause problems. remove it.

#python unittest
python -m unittest discover <dir name>
python -m unittest testfile(module name)
nosetests <directory>
nosetests --ckan --reset-db --with-pylons=test.ini  ckan/tests/lib/test_helpers.py

pip install gunicorn
gunicorn -b 127.0.0.1:9200 -w 5 --paste /etc/ckan/default/development-registry.ini
    --chdir=/home/xxx -n gunicorn-reg --error-logfile /home/xxx/gunicorn-reg.log
    -p /home/xxx/gunicorn-reg.pid --limit-request-line 0 --timeout 60
strace -e open -f <...cmd ...>

#setup no root ckan portal
1. setup apach2 proxy server
 #sudo a2enmod -- enable module
  ^ proxy proxy_http
 # sudo nano /etc/apache2/sites-enabled/000-default.conf
	-- or /etc/httpd/conf.d/vhost..
  <VirtualHost *:80>
    ProxyPreserveHost On

    # Servers to proxy the connection, or;
    # List of application servers:
    # Usage:
    # ProxyPass / http://[IP Addr.]:[port]/
    # ProxyPassReverse / http://[IP Addr.]:[port]/
    # Example:
    ProxyPass /data/ http://127.0.0.1:5001/
    ProxyPassReverse /data/ http://127.0.0.1:5001/

    ServerName localhost

    WSGIScriptAlias /lconlinewsgi/lc.wsgi /var/www/html/wsgi-scripts/cgi-bin/lc.wsgi

    <Directory /usr/local/www/wsgi-scripts>
    Order allow,deny
    Allow from all
    </Directory>

  </VirtualHost>
# https://github.com/wummel/linkchecker

#enable ssl
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/apache2/ssl/apache.key -out /etc/apache2/ssl/apache.crt
sudo nano /etc/apache2/sites-available/default-ssl.conf #change the oil key/crt to new ones
sudo a2ensite default-ssl.conf

 # sudo service restart apache2
2. ckan .ini set -
	ckan.site_url = http://127.0.0.1
	ckan.root_path = /data/{{LANG}}
	ckan.tracking_enabled = false
  link: http://localhost/data/en/dataset
3. test behind load balancer -> (vhost proxy <--> real server)
curl --head 'HOST: open.canada.ca' 'http://varnish.server/data/....'

4. ubuntu maintenance (remove ppa)
ls /etc/apt.d/source.list.d/
	-- remove the unwanted file

5. linux command line tricks
#prepend binary data to a file
printf "\x68\x65\x6c\x6c\x6f\x20\x77\x6f" | cat - /tmp/x  > /tmp/y

#remove last new line from a file
head -c -1 xxx.txt > b.txt

#force wifi renew
sudo iwconfig -a
sudo iwlist <interface> scan

#linux unset environment
unset HTTP_PROXY
unset http_proxy #necessary for GAE run local dev server

#ssh tunnel for psql
ssh -L5555:localhost:5432 username@192.168.80.248 #192.xx is db ip
psql -h localhost -p 5555 -d dbname -U username

#ckan install
# for dcat, need 
pip install -e 'git+https://github.com/open-data/ckanext-dcat.git#egg=ckanext-dcat'
pip install rdflib rdflib_jsonld

#check NIC link up down
#improve virtualbox net stable: Host->Virtualbox->'File'--'Preference'-- add proxy: U:p@host port
$ dmesg | grep -c 'NIC Link is Down'
$ sudo apt-get install lubuntu-desktop
$ gnome-terminal --geometry="135x34+37+95" \
  --tab-with-profile="bw" --working-directory="/home/jffan/src/project" \
  --tab-with-profile="bw" --working-directory="/home/jffan/src/debug"

#get field from text in bash
cat /tmp/x.txt | awk '{ print $1 $4}' > /tmp/b.txt
for line in $(cat a.txt); do wget http://../$line; done

#copy .deb file and repackage
dpkg -x xxx.deb /tmp/somewhere

#add loopback alias
ifconfig lo:0 `dig obd-dev.canadacentral.cloudapp.azure.com a +short`

#tiny visor
Mkdir tinyvisor
Cd tinyvisor
Hg clone http://hg.osdn.jp/view/tinyvisor/tinyvisor-vmm vmm
Hg clone http://hg.osdn.jp/view/tinyvisor/tinyvisor-bios bios


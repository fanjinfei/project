#from exchangelib import DELEGATE, Account, Credentials
from exchangelib import DELEGATE, IMPERSONATION, Account, Credentials, ServiceAccount, \
    EWSDateTime, EWSTimeZone, Configuration, NTLM, CalendarItem, Message, \
    Mailbox, Attendee, Q, ExtendedProperty, FileAttachment, ItemAttachment, \
    HTMLBody, Build, Version

import sys

credentials = Credentials(
    username=sys.argv[1],
    password=sys.argv[2]
)
'''
account = Account(
    primary_smtp_address=sys.argv[1],
    credentials=credentials,
    autodiscover=True,
    access_type=DELEGATE
)
'''
config = Configuration(server=sys.argv[3], credentials=credentials)
account = Account(primary_smtp_address=sys.argv[1], config=config,
                  autodiscover=False, access_type=DELEGATE)
# Print first 100 inbox messages in reverse order
for item in account.inbox.all().order_by('-datetime_received')[:100]:
    print(item.subject, item.body, item.attachments)

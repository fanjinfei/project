# Dev Environment Installation Guide (date:2016-11-18)
## Ubuntu latest LTS (16.04.1 at the writing of this document)

## Table of Contents

* Section 1: prepare Virtualbox
* Section 2: install CKAN from source : branch 2.5
* Section 3: install open-data extensions
* Section 4: create multiple instance
* Section 5 Load database and Run

## Section 1: prepare Virualbox
### 1. download Ubuntu-16.04.1-desktop-amd64.iso from ubuntu.com
### 2. create new VM with 2048MB memory and 30GB storage. Use the downloaded iso as cdrom image.
### 3. start the VM and finish the installation of Ubuntu.

Defaults for every option but don't let it access the internet quite yet till the proxy is configured.  If the install halts looking for something on the internet disable networking in VM's menu bar top right.  The icon is an up and down arrow as of writing this.

### 4. configure VM network proxy for statcan-netb
Assume <user> and <pass> are your credentials for StatCan network B.

#### 4.1. Append file /etc/environment with the following lines:

```
http_proxy="http://<user>:<pass>@stcweb.statcan.ca:80/"
HTTP_PROXY="http://<user>:<pass>@stcweb.statcan.ca:80/"
https_proxy="http://<user>:<pass>@stcweb.statcan.ca:80/"
HTTPS_PROXY="http://<user>:<pass>@stcweb.statcan.ca:80/"
no_proxy="localhost,127.0.0.1,127.0.0.1/8,/var/run/docker.sock"
```

#### 4.2. Create file /etc/apt/apt.conf.d/77proxy with lines:

```
Acquire::http::proxy "http://<user>:<pass>@stcweb.statcan.ca:80/";
Acquire::https::proxy "http://<user>:<pass>@stcweb.statcan.ca:80/";
```

#### 4.3. Update system

```
sudo apt-get update
sudo apt-get upgrade
```

#### 4.4. Install and set Git Proxy:

```
sudo apt-get -y install git
git config --global http.proxy http://<usr>:<pass>@stcweb.statcan.ca:80/
git config --global https.proxy https://<usr>:<pass>@stcweb.statcan.ca:80/ 	
```

#### to unset:

```
git config --global --unset http.proxy
git config --global --unset https.proxy
```
 
### 5. Install addition packages

```
sudo apt-get -y install -o Dpkg::Options::="--force-overwrite" openjdk-8-jdk 
sudo apt-get -y install python-dev postgresql libpq-dev python-pip python-virtualenv git-core solr-jetty redis-server
sudo apt-get -y install libgeos-dev
```

### 6. Restart the VM

This opens up proxy access to Firefox and puts you on the latest of everything for the subsequent updates.

# Section 2 Install CKAN From Canada Branch2.5
## Reference:
* https://github.com/open-data/ckanext-canada
* http://docs.ckan.org/en/latest/maintaining/installing/install-from-source.html
* http://docs.ckan.org/en/ckan-2.5.2/maintaining/solr-multicore.html#creating-another-solr-core

## 1. Install CKAN into a Python virtual Environment
###  1.1 Prepare directory:

```
mkdir -p ~/ckan/lib
sudo ln -s ~/ckan/lib /usr/lib/ckan
mkdir -p ~/ckan/etc
sudo ln -s ~/ckan/etc /etc/ckan
```

###  1.2 Create Python virtual environment

```
sudo mkdir -p /usr/lib/ckan/default
sudo chown `whoami` /usr/lib/ckan/default
virtualenv --no-site-packages /usr/lib/ckan/default
. /usr/lib/ckan/default/bin/activate
```

###  1.3 Checkout open-data ckan branch2.5

```
mkdir -p /usr/lib/ckan/default/src
cd /usr/lib/ckan/default/src
git clone https://github.com/open-data/ckan.git
cd ckan
git checkout canada-v2.5
python setup.py develop
pip install -r requirements.txt
pip install -r dev-requirements.txt
```

###  1.4 re-activate virtual environment

```
deactivate
. /usr/lib/ckan/default/bin/activate
```

## 2. Setup a PostgresSQL database

### 2.1check if the database is installed, with encoding is “UTF-8”: 

```
sudo -u postgres psql -l
```

### 2.2 create a new db user ckan_default:

```
sudo -u postgres createuser -S -D -R -P ckan_default
```

### 2.3 create a new db database ckan_default owned by user ckan_default:

```
sudo -u postgres createdb -O ckan_default ckan_default -E utf-8
```

## 3. Create a CKAN config file
### 3.1 create a directory:

```
sudo mkdir -p /etc/ckan/default
sudo chown -R `whoami` /etc/ckan
sudo chown -R `whoami` ~/ckan/etc
```

### 3.2 create CKAN config file:

```
paster make-config ckan /etc/ckan/default/development.ini
```

### 3.3 edit the development.ini, changing with the following options, replace the following pass with the one in step 2.2.

```
# /etc/ckan/default/development.ini
sqlalchemy.url = postgresql://ckan_default:pass@localhost/ckan_default
ckan.site_id = default
ckan.site_url = http://localhost

* Note: no trailing slash on URL
```

## 4. Setup Solr
### 4.1 edit the jetty8 config :

```
# /etc/default/jetty8
NO_START=0            # (line 4)
JETTY_HOST=127.0.0.1  # (line 16)
JETTY_PORT=8983       # (line 19)
```

### 4.2 restart jetty:

```
sudo service jetty8 restart
```

### 4.3 test from browser with http://localhost:8983/solr/
### 4.4 replace the default schema.xml, and restart solr:

```
sudo mv /etc/solr/conf/schema.xml /etc/solr/conf/schema.xml.bak
sudo ln -s /usr/lib/ckan/default/src/ckan/ckan/config/solr/schema.xml /etc/solr/conf/schema.xml
sudo service jetty8 restart
```

### 4.5 modify the solr_url in development.ini config file:

```
# /etc/ckan/default/development.ini
solr_url=http://127.0.0.1:8983/solr
```

## 5. Create database tables

```
cd /usr/lib/ckan/default/src/ckan
paster db init -c /etc/ckan/default/development.ini  

* You should see Initialising DB:SUCCESS.
```

## 6. Setup Datastore
### 6.1 create datastore user and password:

```
sudo -u postgres createuser -S -D -R -P -l datastore_default
sudo -u postgres createdb -O ckan_default datastore_default -E utf-8
```

### 6.2 modify development.ini options:

```
# /etc/ckan/default/development.ini
ckan.datastore.write_url = postgresql://ckan_default:pass@localhost/datastore_default
ckan.datastore.read_url = postgresql://datastore_default:pass@localhost/datastore_default
```

### 6.3 set permissions:

```
paster --plugin=ckan datastore set-permissions -c /etc/ckan/default/development.ini | sudo -u postgres psql --set ON_ERROR_STOP=1
```

## 7. link to who.ini

who.ini (the repo's .who configuration file) needs to be accessible in the same directory as your CKAN config file, so create a symlink to it:

```
ln -s /usr/lib/ckan/default/src/ckan/who.ini /etc/ckan/default/who.ini
```

## 8. Modify language
modify development.ini option ckan.locale_default from en to en_GB

```
# /etc/ckan/default/development.ini
ckan.locale_default = en_GB
```

## 9. Done for CKAN installation

You can now use the Paste development server to serve CKAN from the command-line. This is a simple and lightweight way to serve CKAN that is useful for development and testing:

```
cd /usr/lib/ckan/default/src/ckan
paster serve /etc/ckan/default/development.ini
```

## 10. test with http://127.0.0.1:5000/


## Section 3 Install open-data extensions

### 1. Recombinant

```
pip install -e git+https://github.com/open-data/ckanext-recombinant.git@wet4#egg=ckanext-recombinant
```

### 2. wet_boew
replace the following `<user>` with your actual username

```
pip install -e 'git+https://github.com/open-data/ckanext-wet-boew.git@wet4-scheming#egg=ckanext-wet-boew'
mkdir -p /home/<user>/wet-boew/v4.0.23
cd /home/<user>/wet-boew/v4.0.23
export WET_VERSION=v4.0.23

mkdir wet-boew && curl -L https://github.com/wet-boew/wet-boew-cdn/archive/$WET_VERSION.tar.gz | tar -zx --strip-components 1 --directory=wet-boew

mkdir GCWeb && curl -L https://github.com/wet-boew/themes-cdn/archive/$WET_VERSION-gcweb.tar.gz | tar -zx --strip-components 1 --directory=GCWeb

mkdir theme-base && curl -L https://github.com/wet-boew/themes-cdn/archive/$WET_VERSION-theme-base.tar.gz | tar -zx --strip-components 1 --directory=theme-base

mkdir theme-gc-intranet && curl -L https://github.com/wet-boew/themes-cdn/archive/$WET_VERSION-theme-gc-intranet.tar.gz | tar -zx --strip-components 1 --directory=theme-gc-intranet

mkdir theme-gcwu-fegc && curl -L https://github.com/wet-boew/themes-cdn/archive/$WET_VERSION-theme-gcwu-fegc.tar.gz | tar -zx --strip-components 1 --directory=theme-gcwu-fegc

mkdir theme-ogpl && curl -L https://github.com/wet-boew/themes-cdn/archive/$WET_VERSION-theme-ogpl.tar.gz | tar -zx --strip-components 1 --directory=theme-ogpl

mkdir theme-wet-boew && curl -L https://github.com/wet-boew/themes-cdn/archive/$WET_VERSION-theme-wet-boew.tar.gz | tar -zx --strip-components 1 --directory=theme-wet-boew
```
Set the  extra_public_paths  settings of the development.ini file to that path where the files are extracted:

```
# /etc/ckan/default/development.ini
extra_public_paths = /home/<user>/wet-boew/v4.0.23
```
### 3. Fluent

```
pip install -e 'git+https://github.com/open-data/ckanext-fluent.git#egg=ckanext-fluent'
```

### 4. Scheming

```
pip install -e 'git+https://github.com/open-data/ckanext-scheming.git#egg=ckanext-scheming'
```

### 5. ckanext-canada

```
pip install -e 'git+https://github.com/open-data/ckanext-canada.git#egg=ckanext-canada'
```

### 6. Ckanapi

```
pip install -e git+https://github.com/ckan/ckanapi.git#egg=ckanapi
```

### 7. googleanalytics

```
pip install -e git+https://github.com/ckan/ckanext-googleanalytics.git#egg=ckanext-googleanalytics
```

### 8. Additional python packages

```
pip install lxml shapely ckantoolkit openpyxl
```

## Section 4 Create multiple instance

Next, create two site, one for portal/public access, one for internal push data (called registry).

#### 1. modify the development.ini to match or include the following:

```
# /etc/ckan/default/development.ini

scheming.dataset_schemas =
    ckanext.canada:schemas/dataset.yaml
    ckanext.canada:schemas/info.yaml

scheming.presets =
    ckanext.scheming:presets.json
    ckanext.fluent:presets.json
    ckanext.canada:schemas/presets.yaml

licenses_group_url = file:///usr/lib/ckan/default/src/ckanext-canada/ckanext/canada/public/static/licenses.json
ckan.i18n_directory = /usr/lib/ckan/default/src/ckanext-canada/build
ckan.auth.create_dataset_if_not_in_organization = false
ckan.activity_streams_email_notifications = false
ckan.datasets_per_page = 10
googleanalytics.id = UA-63144818-1
googleanalytics.account = test.open.canada.ca
```

### 2. create two configuration files names development-portal.ini, development-registry.ini:

```
cp /etc/ckan/default/development.ini /etc/ckan/default/development-registry.ini
cp /etc/ckan/default/development.ini /etc/ckan/default/development-portal.ini
```

### 3. Instance specific config changes
#### 3.1 modify the registry server config file:

```
# /etc/ckan/default/development-registry.ini

ckan.plugins = googleanalytics canada_forms canada_internal canada_public canada_package wet_boew_theme_gc_intranet datastore recombinant scheming_datasets fluent
recombinant.tables = ckanext.canada:recombinant_tables.yaml
```

#### 3.2  modify the portal server config file:

```
# /etc/ckan/default/development-portal.ini

ckan.plugins = googleanalytics canada_forms canada_public canada_package wet_boew_gcweb scheming_datasets fluent
canada.portal_url = http://myserver.com
```

### 4. create two new solr cores, for example:

```
http://localhost:8983/solr/ckan_portal
http://localhost:8983/solr/ckan_registry
```

replace the development-registry.ini file option:

```
# /etc/ckan/default/development-registry.ini

solr_url=http://127.0.0.1:8983/solr/ckan_registry
```

replace the development-portal.ini file option:

```
# /etc/ckan/default/development-portal.ini

solr_url=http://127.0.0.1:8983/solr/ckan_portal
```

#### 4.1 create /usr/share/solr/solr.xml with the following:

```
<solr persistent="true" sharedLib="lib">
    <cores adminPath="/admin/cores">
        <core name="ckan_portal" instanceDir="ckan_portal">
            <property name="dataDir" value="/var/lib/solr/data/ckan_portal" />
        </core>
        <core name="ckan_registry" instanceDir="ckan_registry">
            <property name="dataDir" value="/var/lib/solr/data/ckan_registry" />
        </core>
    </cores>
</solr>
```

#### 4.2 create data directories for the cores:

```
sudo -u jetty mkdir /var/lib/solr/data/ckan_portal
sudo -u jetty mkdir /var/lib/solr/data/ckan_registry
```

#### 4.3 create configuration directory for the cores:

```
sudo mkdir /etc/solr/ckan_portal
sudo cp -r /etc/solr/conf /etc/solr/ckan_portal/
sudo mkdir /etc/solr/ckan_registry
sudo cp -r /etc/solr/conf /etc/solr/ckan_registry/
```

#### 4.4 verify that the link /etc/solr/ckan_portal/conf/schema.xml is to /usr/lib/ckan/default/src/ckan/ckan/config/solr/schema.xml 

#### 4.5 modify /etc/solr/ckan_portal/conf/solrconfig.xml and /etc/solr/ckan_registry/conf/solrconfig.xml option <datadir> to:

```
<dataDir>${dataDir}</dataDir> 
```

#### 4.6 create directory and symlink for conf:

```
sudo mkdir /usr/share/solr/ckan_portal
sudo ln -s /etc/solr/ckan_portal/conf /usr/share/solr/ckan_portal/conf
sudo mkdir /usr/share/solr/ckan_registry
sudo ln -s /etc/solr/ckan_registry/conf /usr/share/solr/ckan_registry/conf
```

#### 4.7 Restart solr:

```
sudo service jetty8 restart
```

#### 4.8 Test: http://localhost:8983/solr you should see multiple cores admin link.

## Section 5 Load database and Run

### 1. build translations:

```
cd /usr/lib/ckan/default/src/ckanext-canada
bin/build-combined-ckan-mo.sh
```

### 2. Correct languages

In both the registry and portal development.ini files:

```
# /etc/ckan/default/development-registry.ini
# and
# /etc/ckan/default/development-portal.ini

ckan.locale_default = en
```

### 3. initializing both databases for two sites:
```
cd /usr/lib/ckan/default/src/ckan
sudo -u postgres createdb -O ckan_default ckanregistry -E utf-8
sudo -u postgres createdb -O ckan_default ckanportal -E utf-8
```

#### 3.1 modify the sqlalchemy_url in development-registry.ini with your password from earlier

```
# /etc/ckan/default/development-registry.ini

sqlalchemy.url = postgresql://ckan_default:pass@localhost/ckanregistry
```

#### 3.2 and development-registry.ini with your password from earlier

```
# /etc/ckan/default/development-portal.ini

sqlalchemy.url = postgresql://ckan_default:pass@localhost/ckanportal
```

#### 3.3 Initialize

```
cd /usr/lib/ckan/default/src/ckan
paster db init -c /etc/ckan/default/development-portal.ini
paster db init -c /etc/ckan/default/development-registry.ini
```

### 4. Load organizations:

```
ckanapi load organizations -I /usr/lib/ckan/default/src/ckanext-canada/transitional_orgs.jsonl -c /etc/ckan/default/development-registry.ini
ckanapi load organizations -I /usr/lib/ckan/default/src/ckanext-canada/transitional_orgs.jsonl -c /etc/ckan/default/development-portal.ini
```

### 5. if you want to run both sites in the same time, modify port number in one of the configuration files:

```
# /etc/ckan/default/development-portal.ini

port = 5001
```

### 6. create recombinant, (need a least 2G free memory):

```
cd /usr/lib/ckan/default/src/ckanext-recombinant
paster recombinant create -a -c /etc/ckan/default/development-registry.ini
```

### 7. Now start both sites with:

```
cd /usr/lib/ckan/default/src/ckan
paster serve /etc/ckan/default/development-portal.ini&
paster serve /etc/ckan/default/development-registry.ini&
```

### 8. Test: http://localhost:5000 and http://localhost:5001

# Author: @Fan 
# Review / Transcription to Wiki: @MrMajewski

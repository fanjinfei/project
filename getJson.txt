
	

$.getJSON is a shorthand for
$.ajax({
    dataType: "json",
    url: url,
    data: data,
    success: success
});

# with header (for auth)
So you can simply use it directly like 
$.ajax({
    beforeSend: function(request) {
        request.setRequestHeader("X-Mashape-Key", 'key_here');
    },
    dataType: "json",
    url: settings.apiPath + settings.username + '/boards/',
    success: function(data) {
        //Your code
    }
});

#with data
var data = {
    SomeID: "18",
    Utc: null,
    Flags: "324"
};

$.getJSON("https://somewhere.com/AllGet?callback=?", data, function (result) {
    alert(result);
});


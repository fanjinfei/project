#!/usr/bin/env python3

import sys
# from urllib2 import urlopen # Python 2
from urllib.request import urlopen # Python 3
import urllib
import urllib.parse as urlparse
import shlex
from collections import defaultdict
from datetime import datetime, timedelta

'''
    weglog_parser.py [<04/Jul/2017> | <14d>] log_file1 log_file2 ...
'''

class WebLog():
    def __init__(self):
        self.data = defaultdict(int)
        self.start = False
    def reset(self):
        self.start = False
    def read_line(self, line, start_date):
        if not self.start:
            if line.find(start_date) > 0:
                self.start = True
        if not self.start:
            return
        if line.find('fgp') < 0: return

        d = self.data
        i1 = line.find(' - - [')
        if i1 < 0: return
        i2 = line.find('] ', i1+6)
        if i2 < 0: return
        line = line[i2+2:]
        line = shlex.split(line)[0]
        
        ds = line.split()
        if ds[0] != 'GET': return
        
        url, url2 = ds[1], None
        #  /data/fr/dataset?q%2525253Dmaple%253D%2526subject%253Dgovernment_and_politics%2526keywords%253DCanada%2526subject%253Deconomics_and_industry%2526collection%253Dfgp=&portal_type=info&_res_type_limit=0&_organization_limit=0
        while True:
            #url2 = urllib.unquote(url)
            url2 = urlparse.unquote(url)
            if url2 == url:
                break
            url = url2
        if url.find('collection=fgp') < 0: return
        
        i3 = url.find('&q=')
        if i3 < 0:
            i3 = url.find('?q=')
        if i3 < 0: return
        if i3+3 == len(url): return
        
        q = url[i3+3:]
        i4 = q.find('&')
        if i4 > 0:
            q = q[:i4]
        else:
            return
        try:
            qs = shlex.split(q.replace('+', ' '))
        except ValueError:
            qs = [q]
        
        qs = [q.strip('-"\',.()[]=`~!@#_+;{}').lower() for q in qs]
        qs = [q for q in qs if len(q)>=2]
        for q in qs:
            d[q] += 1
        
    def stats(self):
        data = [(k,v) for k,v in self.data.items() ]
        data.sort(key = lambda x: -x[1])
        for i in range(0, 20):
            print (data[i])
def main():
    log = WebLog()
    start_date = sys.argv[1]
    if start_date[-1] == 'd':
        days = int(start_date[:-1])
        start_date = (datetime.now() - timedelta(days=days)).strftime("%d/%b/%Y")

    for fname in sys.argv[2:]:
        log.reset()
        with open(fname, 'rb') as f:
            for line in f:
                log.read_line(line.decode('utf-8'), start_date)
    log.stats()
    
main()
